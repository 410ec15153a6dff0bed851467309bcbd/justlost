//Todo Voir https://rakibul.net/fcm-web-js

importScripts('https://www.gstatic.com/firebasejs/7.14.5/firebase-app.js')
importScripts('https://www.gstatic.com/firebasejs/7.14.5/firebase-messaging.js')

var config = {
    apiKey: "AIzaSyAWMCXX-r9hwbQ-fMI8XJSOXp-K9e8f1Vs",
    projectId: "justlost-f169e",
    messagingSenderId: "810377215216",
    appId :  "1:810377215216:web:14b9d5fd7b7f759f889635"
};
firebase.initializeApp(config);

// Retrieve an instance of Firebase Data Messaging so that it can handle background messages.
const messaging = firebase.messaging()
messaging.setBackgroundMessageHandler(function(payload) {
  const notificationTitle = 'Data Message Title';
  const notificationOptions = {
    body: 'Data Message body',
    icon: 'alarm.png'
  };
  
  return self.registration.showNotification(notificationTitle,
      notificationOptions);
});