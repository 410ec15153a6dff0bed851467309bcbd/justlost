var { Firebase } = require('./firebase');
//Ne pas toucher au code pour instancier firebase
const firebase = new Firebase()
exports.firebase = firebase

const routing = require('../functions/services/express-routing.js')

module.exports = routing;
