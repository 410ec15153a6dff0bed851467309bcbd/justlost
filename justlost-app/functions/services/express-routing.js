const cors = require("cors")
const express = require("express")
var createError = require('http-errors');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');


const elasticRouter = require('../routes/elastic');
const databaseRouter = require('../routes/database');
const notificationRouter = require('../routes/notification');

const app = express();
// Automatically allow cross-origin requests
app.use(cors({ origin: true }));
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

/**Add Routes
 * 
 */
app.use('/elastic', elasticRouter);
app.use('/database', databaseRouter);
app.use('/notification', notificationRouter);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
  });
  
  // error handler
  app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
  
    // render the error page
    res.status(err.status || 500);
    res.render('error');
  });

module.exports = app

