const { Client } = require('@elastic/elasticsearch')
const firebase = require('./get-firebase')
const { getDeclarationDocument } = require('./database-common')

//A modifier
const ELASTIC_SEARCH_URL = process.env.ELASTIC_SEARCH_URL || "http://34.78.130.5:9200/"
const client = new Client({ node: ELASTIC_SEARCH_URL })


/**
 * List des fonctions exportéess
 */

exports.indexOneInES = indexOneInES
exports.indexAllInES = indexAllInES
exports.removeIndexInES = removeIndexInES
exports.indexNewDocumentInES = indexNewDocumentInES
//*******************************************************************************


/**
 * Create the document to store in Elastic
 * @param {object} document 
 */
function mapFirebaseDocumentToElastic(document) {
  let esDocument = {}
  const tags = document.tags.filter(([tag, action]) => action != 'not_used')
  esDocument.declarationId = document.declarationId
  esDocument.declarationType = document.declarationType
  esDocument.categoryId = document.category.id
  esDocument.categoryCode = document.category.code
  esDocument.tagsHigh = document.tagsHigh
  esDocument.tags = tags.filter(([tag, action]) => action === 'used').map(([tag, action]) => tag)
  esDocument.userDescription = document.userDescription // On le met qd même
  esDocument.eventDate = document.eventDate && document.eventDate.stringDate
  esDocument.latitude = document.eventZone && document.eventZone.userSelectionArea.latitude
  esDocument.longitude = document.eventZone && document.eventZone.userSelectionArea.longitude
  return esDocument
}

/**
 * Index un document RTDB dans ES
 * @param {*} esIndex Nom de l'index dans ES
 * @param {*} document Document RTDB qui sera transformé en document ES
 */


function indexOneInES(document, esIndex) {
  const esDocument = mapFirebaseDocumentToElastic(document)
  return pushDocumentToElastic(esIndex, esDocument)
}

/**
 * Envoi un document à ES
 * @param {string} esIndex Nom de l'index dans ES
 * @param {*} esDocument Corps du document ES
 */
async function pushDocumentToElastic(esIndex, esDocument) {
  return await client.index({
    index: esIndex,
    body: esDocument
  })
}

/**
 * Réindex tous les documents de la collection Firebase DBRT passée en paramètre dans l'index Elastic passé en paramètre.
 * 
 * @param {Firebase application} firebase 
 * @param {String} esIndex 
 * @param {String} collection 
 */
async function indexAllInES(firebase, param) {
  const {collection, esIndex} = param
  //Remove the index first if its exists.
  removeIndexInES(firebase, {esIndex})

  //console.log('firebase.database()', firebase.database())
  await firebase.database().ref(collection)
    .once('value', async snapshot => {
      const documentsObject = snapshot.val();
      let documents;
      if (documentsObject) {
        documents = Object.keys(documentsObject).map(key => ({
          ...documentsObject[key],
          id: key,
        }))
        documents.map(document => {
          return indexOneInES(document, esIndex)
        })
        await client.indices.refresh({ index: esIndex })
      }
    }
    )
  return {result: 'ok'}
}

async function removeIndexInES(firebase, {esIndex}) {
  //console.log('esIndex', esIndex)
  //Remove the index first if its exists.
  try {
    await client.indices.delete({ index: esIndex, ignore_unavailable: true })
    return { result: 'ok' }
  } catch (e) { console.log("Erreur lors de la suppression de l'index", e); return { result: 'ko' } }
}

 function indexNewDocumentInES (firebase, snapshot, context) {
  const declaration = snapshot.val();
  return indexOneInES(declaration, 'declarations')
  .then(async ()=>{
    //console.log('débute')
    //const {id} = declaration // A changer par declarationId
    const {declarationId} = declaration
    //console.log('declarationId', declarationId)
    const {declarationRef} = await getDeclarationDocument(firebase,declarationId)
    //console.log('declarationRef', declarationRef)
    return await declarationRef.update({pushToES:true})
  })
  .catch(error => error)
 }


