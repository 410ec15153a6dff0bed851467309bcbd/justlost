'use strict'
const { getDeclarationDocument } = require('./database-common')
const { getESQuery } = require('../config/ES')

/**
 * List des fonctions exportéess
 */
exports.updateMatchingList = updateMatchingList
exports.updateDecisionForAMatch = updateDecisionForAMatch
exports.updateMatchedLostDeclarationList = updateMatchedLostDeclarationList
exports.updateAllDeclarationMapping = updateAllDeclarationMapping
//*******************************************************************************

async function updateMatchingList(firebase, param) {
  let resultError = []
  const { declaration } = param

  await getMatchingInESForDeclaration(declaration)
    .then(async matchingEs =>
      await refreshDeclarationMachingInRTDB(firebase, declaration, matchingEs)
        .catch(error => resultError.push(error))
    )
    .catch(error => resultError.push(error))

  //Nous allons lancer le mécanisme de décision automatique à ce niveau
  await automaticDecision(firebase, declaration.declarationId)


  if (resultError.length > 0)
    return Promise.reject({ message: `Fail to update Mapping list for declation ${declaration.declarationId}.`, resultError })
  else
    return Promise.resolve({ message: `Mapping list for declation ${declaration.declarationId} successfuly updated!` })
}

/**
 * Envoi autoamtique une notification si un nouveau matching dépasse le scoring configuré
 */
async function automaticDecision(firebase, declarationId) {
  const { declaration, declarationRef } = await getDeclarationDocument(firebase, declarationId)
  if (!declaration.esMatchingList)
    return null

  const esMatchingListArray = Object.values(declaration.esMatchingList) //On ne doit rechercher QUE dans esMatchingList
  const maxScoreMatching = getMaxScore(esMatchingListArray)
  const triggerScore = await getConfigScore(firebase, declaration.category.code)
  if (maxScoreMatching.score >= triggerScore) {
    let resultError = []
    const { foundDeclarationId } = maxScoreMatching
    const param = { declarationId, matchId: foundDeclarationId, decision: 'toNotificateAndMatch', decisionOrigin: 'system' }

    await updateDecisionForAMatch(firebase, param)

  }
  return Promise.resolve('ok')
}

function getMaxScore(data) {
  return data.reduce((max, p) => p.score > max.score ? p : max, data[0]);
}

async function getConfigScore(firebase, category) {
  const configRef = firebase.database().ref(`config/${category}`)
  let configScoreDetail, configTriggerScore = 100
  await configRef
    .once('value')
    .then(snapshot => {
      configScoreDetail = snapshot.val();
      configTriggerScore = configScoreDetail.triggerScore
    })
  return configTriggerScore
}



/**
 * Retourne la liste des déclarations pouvant matcher avec un autre
 */

async function getMatchingInESForDeclaration(declaration) {
  const { Client } = require('@elastic/elasticsearch')
  const client = new Client({ node: process.env.ELASTIC_SEARCH_URL })

  //Récupèration de tous les critères de recherche
  const criteria = getESCriteria(declaration)
  //console.log(JSON.stringify(criteria))
  const { body } = await client.search(criteria)
    .catch(err => console.log(err))
  //TODO : Gestion d'erreur

  //On contruit une liste avec uniquement les données pertinantes.
  //DeclarationId, esId, score
  const hits = body.hits.hits
  //const hitsFormatted = hits.map(hit => {return {declarationId:hit._source.declarationId||1, esId:hit._id, score:_score}})
  const hitsFormatted = hits.map(hit => ({
    declarationId: hit._source.declarationId,
    esId: hit._id,
    score: hit._score
  }))
  return hitsFormatted
}

function getESCriteria(declaration) {
  const query = getESQuery(declaration)
  return query
}

async function refreshDeclarationMachingInRTDB(firebase, declaration, matchingES) {
  const { declarationId, declarationType } = declaration
  let declarationRecord

  const declarationRef = firebase.database().ref(`declarations/${declarationId}`)
  await declarationRef
    .once('value')
    .then(snapshot => {
      declarationRecord = snapshot.val();
    })

  const adminMatchingList = declarationRecord.adminMatchingList || []


  //On aura 2 listes de macthings.
  //Une liste matching ES et une liste matchingAdmin
  //la liste des matchingAdmin est immuable


  //On va mettre à jour la liste des matchings ES selon plusieurs critères
  //si un match a été touché par le gestionnaire, on le laisse, on ne le met à pas à jour
  //Sauf si des nouvelles données ont fait qu'il a évolué, par example plus de détail qui fait qui match mieux.
  //On pourrait comparer le score et s'il a évolué en positif, on stock un message sur le matching pour le gestionnaire.
  //Ce message s'affichera a coté du match et le gestionnaire pourra le marqué comme vu.


  //On commence par supprimer tous les matching ES de la déclaration.
  //on y copie seulement ceux venant d'ES qui n'existent pas dans mappingAdmin.
  //ceux qui existent dans mappingAdmin avec un scoring différent (à voir si sela suffit ou bien s'il faut le faire sur son HASH)
  // => cela signifie qu'une autre déclaration a matchée ou que la déclaration matchée est fermé ou close.
  let matchToAdd = []
  let matchToUpdate = []
  //Format des listes de matching en rtdb 
  // [[id]] :{}

  matchingES.forEach(match => {
    const associatedDeclarationId = match.declarationId
    //Existe en matchingAdmin ?

    if (associatedDeclarationId) {
      if (adminMatchingList[associatedDeclarationId])
        matchToUpdate.push(match)
      else
        matchToAdd.push(match)
    }
  });

  const matchToAddInRTDB = constructRTDBMatchDocument({ declarationId, declarationType }, matchToAdd)

  //On met à jour la liste matchingES
  return await declarationRef.update({ 'esMatchingList': matchToAddInRTDB })
}

function constructRTDBMatchDocument({ declarationId, declarationType }, matchToAdd) {
  let matchToAddInRTDB = {}
  matchToAdd.forEach(matchES => {
    let matchItem = {
      lostDeclarationId: declarationType === 'lost' ? declarationId : matchES,
      foundDeclarationId: declarationType === 'lost' ? matchES.declarationId : declarationType,
      esId: matchES.esId,
      score: matchES.score,
      decision: "waiting_decision",
      decisionOrigin: "none",
    }
    matchToAddInRTDB[matchES.declarationId] = matchItem
  });
  return matchToAddInRTDB
}

//************************************
/**
 * Retourne un object TRDB.Reference correspondant au matching d'une declaration.
 * @param {*} declarationId 
 * @param {*} matchId 
 */
async function getRTDBMatchRef(firebase, declarationId, matchId) {
  let matchRef, matchRefKey
  const adminMatchingRef = firebase.database().ref(`declarations/${declarationId}/adminMatchingList/${matchId}`)
  await adminMatchingRef.once("value")
    .then(snapshot => {
      if (snapshot.exists()) {
        matchRef = adminMatchingRef
        matchRefKey = snapshot.key
      }
    })
  if (!matchRef) {
    const esMatchingRef = firebase.database().ref(`declarations/${declarationId}/esMatchingList/${matchId}`)
    await esMatchingRef.once("value")
      .then(snapshot => {
        if (snapshot.exists()) {
          matchRef = esMatchingRef
          matchRefKey = snapshot.key
        }
      })
  }
  if (!matchRef)
    console.log('ERR : Pas de matching trouvé pour declarationId/matchId', declarationId, matchId)

  return { matchRef, matchRefKey }
}

/**
 * Met à jour la décision d'un match pour une déclaration.
 * Les prop suivants doivent être dans l'objet param passé.
 * @param {*} declarationId 
 * @param {*} matchId 
 * @param {*} decision 
 * @param {*} decisionOrigin 
 */
async function updateDecisionForAMatch(firebase, param) {
  let resultError = []
  const { declarationId, matchId, decision, decisionOrigin } = param
  let { matchRef, matchRefKey } = await getRTDBMatchRef(firebase, declarationId, matchId)
  let newData = { decision, decisionOrigin }

  let notificationData
  if ((decision === 'toNotificate') || (decision === 'toNotificateAndMatch'))
    notificationData = await getNotificationData(firebase, matchRef, param)

  //si le match était dans esMatchingList, alors on le déplace dans adminMappingList
  if (matchRef.parent.key === 'esMatchingList') {
    await matchRef.once('value')
      .then(async snapshot => {
        let matchDetail = snapshot.val();

        matchDetail = { ...matchDetail, ...newData } //On met à jour la décision
        //Supprimer l'entrée
        await matchRef
          .remove()
          .catch(error => resultError.push(error))

        if ((decision === 'toNotificate') || (decision === 'toNotificateAndMatch')) //On ajoute comme cela la noticication en même temps, pour notifier
          matchDetail.notificationList = {"auto_notification" :notificationData}
        //on l'ajoute dans la partie adminMappingList
        await firebase.database().ref(`declarations/${declarationId}/adminMatchingList/${matchRefKey}`)
          .set(matchDetail)
          .catch(error => resultError.push(error))
      })
  }
  else {
    await matchRef.update(newData)
      .then(async () => {
        if ((decision === 'toNotificate') || (decision === 'toNotificateAndMatch')) {
          await firebase.database().ref(`declarations/${declarationId}/adminMatchingList/${matchRefKey}/notificationList`)
            .push(notificationData) // On l'ajoute au tableau existant pour notifier
            .catch(error => resultError.push(error))
        }
      })
      .catch(error => resultError.push(error))
  }

if ((decision == 'matched') || (decision === 'toNotificateAndMatch'))
{
  const  declarationMatched = await (await getDeclarationDocument(firebase, matchRefKey)).declaration
  const foundDetail = { 
      fullName : declarationMatched.declarationOwner.FullName || declarationMatched.declarationOwner.fullName,
      email : declarationMatched.declarationOwner.email,
      message : declarationMatched.message || "Aucun message"
  }
  await firebase.database().ref(`declarations/${declarationId}/foundDetail`).set(foundDetail)
  await firebase.database().ref(`declarations/${declarationId}/status`).set('matched')
}


if (resultError.length > 0)
  return Promise.reject({ message: `Fail to update decision on matching ${matchId} for declation ${declarationId}.` })
else
  return Promise.resolve({ message: `Decision on matching ${matchId} for declation ${declarationId} successfuly updated!` })
  
}


async function getNotificationData(firebase, matchRef, { declarationId, matchId, decision, decisionOrigin }) {
  const { declaration, declarationRef } = await getDeclarationDocument(firebase, declarationId)
  const userId = declaration.declarationOwner.userId
  const userDescription = declaration.userDescription
  const eventStringDate = new Intl.DateTimeFormat('fr-FR').format(declaration.eventDate.stringDate)
  const eventZoneUserInput = declaration.eventZone.userInput
  const imageURL = declaration.imageURL
  const sendTimeStamp = Date.now()
  const sendGMTStringDate = new Date().toGMTString()

  return Promise.resolve({ userId, userDescription, eventStringDate, eventZoneUserInput, imageURL, sendTimeStamp, sendGMTStringDate })
}


async function updateMatchedLostDeclarationList(firebase, body) {

  if (!body.declaration || !body.declaration.declarationId)
    return Promise.reject("Missing object declaration with declarationId field !")

  let declaration
  const declarationId = body.declaration.declarationId
  const declarationRef = firebase.database().ref(`declarations/${declarationId}`)
  await declarationRef.once("value")
    .then(snapshot => {
      if (snapshot.exists())
        declaration = snapshot.val()
    })

  const declarationType = declaration.declarationType

  if (declarationType === 'found') {

    return await getMatchingInESForDeclaration(declaration)
      .then(async (matchingEs) => {


        matchingEs.forEach(async item => {
          let declarationMatched
          const declarationMatchedId = item.declarationId
          const declarationMatchedRef = firebase.database().ref(`declarations/${declarationMatchedId}`)
          await declarationMatchedRef.once("value")
            .then(snapshot => {
              if (snapshot.exists())
                declarationMatched = snapshot.val()
            })

          const updateOk = await updateMatchingList(firebase, { declaration: declarationMatched })
          //console.log('updateOK', updateOk)

        })

        return Promise.resolve('OK !')

      })
  }
  else
    return Promise.reject('KO')
}

async function updateAllDeclarationMapping(firebase, body) {
  let declarationList
  const declarationListRef = firebase.database().ref(`declarations`)
  await declarationListRef.once("value")
    .then(snapshot => {
      if (snapshot.exists())
        declarationList = snapshot.val()
    })
  Object.values(declarationList).forEach(async declaration => {
    if (declaration.declarationType === 'lost') {
      //console.log('Upating ', declaration.declarationId)
      await updateMatchingList(firebase, { declaration })
        .then(() => console.log('Update OK matching of ', declaration))
        .catch(error => console.log('Update KO matching of ', declaration, error))
    }
  });
  return Promise.resolve('OK !')
}