
/**
 * List des fonctions exportéess
 */
exports.sendNotification = sendNotification

async function sendNotification(firebase, param) {
    Array.prototype.unique = function() {
        return this.filter(function (value, index, self) { 
          return self.indexOf(value) === index;
        });
      }
    const { userId } = param
    let user
    //On va récupérer les FCM ed l'uutilisateur pour lui envoyer une notification sur tous ses devices.
    const userRef = firebase.database().ref(`users/${userId}`)
    await userRef
        .once('value')
        .then(snapshot => {
            user = snapshot.val();
        })

    if (!user)
    {
        //console.log("Pas d'utilisateur trouvé", param)
        return Promise.reject("Pas d'utilisateur trouvé")
    }

    if (!user.fcmIdList)
        return Promise.reject("Pas de token FCM")

    const registrationTokens = Object.values(user.fcmIdList).map(fcm => fcm.fcmId).unique()
    //console.log('registrationTokens', registrationTokens)

    const message = {
        data: {
            title: param.title,
            description: param.description,
            link: param.link || "",
            image: param.image
        },
        tokens: registrationTokens,
    }

    console.log('message', message)
    await firebase.messaging().sendMulticast(message)
        .then((response) => {
            const msg = response.successCount + ' messages were sent successfully'
            console.log(msg);
            return Promise.resolve(msg)

        })
        .catch(error => {
            const msg = 'Fail to send messages !'
            console.log(msg);
            return Promise.reject(msg)
        })
}