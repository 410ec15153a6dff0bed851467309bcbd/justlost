/**
import EXIF from "exif-js";
/**
 * Convert detail GPS point  (degres/minutes/Secondes) to GPS decimal degrees
 * @param {array} latitude 
 * @param {array} longitude 
 */
function convertToGPS(latitude, longitude) {
    //const latitude = [50,33,23.248079]
    //const longitude = [3,4,23.388959]

    //The rules is to set all in degres unit

    //const lat_ref = 'N'
    const lat_s = latitude[0] //Degres
    const lat_m = latitude[1] //Minutes
    const lat_v = latitude[2] //Secondes

    //const lon_ref = 'E'
    const lon_s = longitude[0] //Degres
    const lon_m = longitude[1] //Minutes
    const lon_v = longitude[2] //Secondes

    const gps = [
        lat_s + lat_m / 60.0 + lat_v / 3600.0,
        lon_s + lon_m / 60.0 + lon_v / 3600.0];

    console.log('calculated GPS : ', gps)
    return gps
}

export function getExifGPS(file) {
    if (file) {
        EXIF.getData(file, function () {
            var exifData = EXIF.pretty(this);
            if (exifData) {
                console.log('Raw EXIF :', exifData)
                console.log('EXIF.getTag(this, "GPSLatitude")', EXIF.getTag(this, "GPSLatitude"))
                const gps = convertToGPS(
                    EXIF.getTag(this, "GPSLatitude"),
                    EXIF.getTag(this, "GPSLongitude"))

                return gps
            }
        })
    }
    return null
}
