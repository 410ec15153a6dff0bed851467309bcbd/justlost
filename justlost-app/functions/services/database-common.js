/**
 * List des fonctions exportéess
 */
exports.getDeclarationDocument = getDeclarationDocument

//*******************************************************************************

async function getDeclarationDocument(firebase, declarationId){
    let declaration
    try{
    //console.log('On est ici', declarationId)
    const declarationRef = firebase.database().ref(`declarations/${declarationId}`)
    //console.log('On est après', declarationRef)
  await declarationRef.once("value")
    .then(snapshot => {
      if (snapshot.exists())
        declaration = snapshot.val()
    })
    return {declarationRef, declaration}
}catch(e)
{console.log('ERRRRRREUR : ', e)}
}