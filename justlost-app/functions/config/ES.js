//Recherche les déclarations de type inverses qui peuvent matchées
exports.getESQuery = (declaration) => {
    const { declarationType } = declaration

    //TODO : On doit rechercher des déclarations opposées
    const searchType = declarationType === "lost" ? "found" : "lost"
    const categoryId = declaration.category.id
    const tagsOR = declaration.tags //pour avoir un format comme "tags:peluche OR tags:taupe"
    .filter(([tag, action]) => action === 'used')
    .map(([tag, action]) => `tags:${tag}`)
    .join(" OR ")
    
    const query = {
        query: {
            function_score: { //Permet de scorer le résultat comme l'on veut
                query: {
                    bool: {
                        should: [ // Souhait de contenance, l'utilsiation "must" est possible, à voir                            
                        {
                                query_string: {
                                    query: tagsOR
                                }
                            }

                        ]
                        ,
                        filter: [
                            { term: { declarationType: searchType } },
                            { term: { categoryId: categoryId } }
                        ]
                    }
                }
                ,
                functions: [
                    {
                        filter: { match: { tags: "gants" } }, //Ici on pourra mettre un tag tres fort pour avoir un mot avec un poids plus fort
                        weight: 23
                    }]
                ,
                boost: "5",
                score_mode: "max",
                boost_mode: "multiply",
                min_score: 1
            }
        }
    }


    return {
        index: 'declarations',
        body: {
            ...query
        }
    }
}
