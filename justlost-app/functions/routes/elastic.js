var express = require('express');
var router = express.Router();

const {
    indexAllInES,
    removeIndexInES } = require('../services/elastic-indexation')

/**
 * Réindex tous les documents d'une collection RTDB dans ES
 * Exemple d'appel : https://europe-west1-justlost-f169e.cloudfunctions.net/express/elastic/declarations/reindex/all/d7
 */
router.get('/:collection/reindex/all/:index?',
    (req, res) => indexAllInES({
        collection: req.params.collection,
        esIndex: req.params.index || req.params.collection
    }, res)
        .then(result => { res.set('Access-Control-Allow-Origin', '*'); res.status(201).json(result) })
        .catch(error => { res.set('Access-Control-Allow-Origin', '*'); res.status(400).json(error) })
)
/**
 * Supprime un index dans ES
 */
router.delete('/index/remove/:esIndex?',
    (req, res) => removeIndexInES({
        esIndex: req.params.esIndex
    }, res)
        .then(result => { res.set('Access-Control-Allow-Origin', '*'); res.status(200).json(result) })
        .catch(error => { res.set('Access-Control-Allow-Origin', '*'); res.status(400).json(error) })
)

module.exports = router;
