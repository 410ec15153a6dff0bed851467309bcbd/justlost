var express = require('express');
var router = express.Router();
const DECLARATION_TEST = "-M7wqG-Ih6NH8_8QUutT" //Todo à enlever

const {
    updateMatchingList,
    updateMatchedLostDeclarationList, 
    updateDecisionForAMatch,
    updateAllDeclarationMapping} = require('../services/database-updating')

router.post('/matching/decision/update', async function (req, res, next) {
    //TODO : plugger avec l'API au lieu de mettre en dur
    const body = req.body.matchId ? req.body : {
        declarationId: DECLARATION_TEST,
        matchId: "-M7woA2neStH6vcGFp6w",
        decision: "matched",
        decisionOrigin: 'admin'
    } // req.body
    const {firebase} = require('../services/get-firebase')
    updateDecisionForAMatch(firebase, body)
    .then(result => {res.set('Access-Control-Allow-Origin', '*');res.status(200).json(result)})
    .catch(error => {res.set('Access-Control-Allow-Origin', '*');res.status(400).json(error)})
})

router.post('/matching/update/all', async function (req, res, next) {
    //Todo :retirer le dur
    const {declarationId} = req.body.declaration 
    const {firebase} = require('../services/get-firebase')
    const declarationRef = firebase.database().ref('/declarations/' + declarationId )
    let declaration
    await declarationRef.once('value').then(function (dataSnapshot) { declaration = dataSnapshot.val() })
    await updateMatchingList(firebase, {declaration})
    .then(result => {res.set('Access-Control-Allow-Origin', '*');res.status(200).json(result)})
    .catch(error => {res.set('Access-Control-Allow-Origin', '*');res.status(400).json(error)})
})

router.get('/newDeclarationFound', async function (req, res, next) {
    //Todo :retirer le dur
    const body = req.body
    const {firebase} = require('../services/get-firebase')
    await updateMatchedLostDeclarationList(firebase, body)
    .then(result => {res.set('Access-Control-Allow-Origin', '*');res.status(200).json(result)})
    .catch(error => {res.set('Access-Control-Allow-Origin', '*');res.status(400).json(error)})
})

router.get('/updateAllDeclarationMapping', async function (req, res, next) {
    //Todo :retirer le dur
    const body = req.body
    const {firebase} = require('../services/get-firebase')
    await updateAllDeclarationMapping(firebase, body)
    .then(result => {res.set('Access-Control-Allow-Origin', '*');res.status(200).json(result)})
    .catch(error => {res.set('Access-Control-Allow-Origin', '*');res.status(400).json(error)})
})



module.exports = router;
