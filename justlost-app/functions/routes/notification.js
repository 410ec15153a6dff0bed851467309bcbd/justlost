var express = require('express');
const { sendNotification } = require('../services/notification')
var router = express.Router();



router.post('/send', async function (req, res, next) {
    
    const {firebase} = require('../services/get-firebase')
    await sendNotification(firebase, req.body)
    .then(result => {res.set('Access-Control-Allow-Origin', '*');res.status(200).json(result)})
    .catch(error => {res.set('Access-Control-Allow-Origin', '*');res.status(400).json(error)})
})


module.exports = router;
