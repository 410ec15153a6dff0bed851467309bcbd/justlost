const dotenv = require('dotenv')
dotenv.config();
const cors = require('cors')({ origin: true });
const functions = require('firebase-functions');
const { setFirebase } = require('./services/get-firebase')
const {sendNotification} = require('./services/notification')
const admin = require('firebase-admin');

admin.initializeApp();

//exports.filebase = admin

const routing = require('./services/express-routing')
const {
  indexAllInES,
  removeIndexInES,
  indexNewDocumentInES } = require('./services/elastic-indexation')


const {
  updateMatchingList,
  updateDecisionForAMatch,
  updateMatchedLostDeclarationList,
  updateAllDeclarationMapping } = require('./services/database-updating')

//Export tous les API REST routés
exports.express = functions.region(process.env.REACT_APP_FUNCTIONS_REGION).https.onRequest(routing);



exports.onCreateDeclaration = functions.region(process.env.REACT_APP_FUNCTIONS_REGION)
  .database.ref('/declarations/{pushId}').onCreate(
    (snapshot, context) => {
      return new Promise((resolve, reject) => {
        indexNewDocumentInES(admin, snapshot, context)
          .then(res => resolve(res))
          .catch(error => reject(error))
      })
    })

exports.helloWorld = functions.region(process.env.REACT_APP_FUNCTIONS_REGION).https.onRequest((request, response) => {
  response.send("Hello from Firebase!");
});

exports.createConfig = functions.region(process.env.REACT_APP_FUNCTIONS_REGION)
  .https.onCall(
  (data, context) => {
    //console.log(data)
    return new Promise(async (resolve, reject) => {
      await admin.database().ref('/config')
      .set(data)
        .then(res => resolve(res))
        .catch(error => reject(error))
    })
  })

exports.updateDecision = functions.region(process.env.REACT_APP_FUNCTIONS_REGION)
  .https.onCall(
    (data, context) => {
      return new Promise(async (resolve, reject) => {
        await updateDecisionForAMatch(admin, data)
          .then(res => resolve(res))
          .catch(error => reject(error))
      })
    })
/*
 * Ici sont définis les méthodes pour les API
 * @param {*} request 
 * @param {*} response 
 */
exports.indexAllInES = functions.region(process.env.REACT_APP_FUNCTIONS_REGION)
  .https.onCall(
    (data, context) => {
      console.log("indexAllInES avec data : data")
      return new Promise(async (resolve, reject) => {
        return await indexAllInES(admin, { esIndex: data.collection, ...data })
          .then(res => resolve(res))
          .catch(error => reject(error))
      })
    })

exports.removeIndexInES = functions.region(process.env.REACT_APP_FUNCTIONS_REGION)
  .https.onCall(
    (data, context) => {
      console.log("removeIndexInES avec data : data")
      return new Promise((resolve, reject) => {
        removeIndexInES(admin, data)
          .then(res => resolve(res))
          .catch(error => reject(error))
      })
    })

exports.onPushToES = functions.region(process.env.REACT_APP_FUNCTIONS_REGION)
  .database.ref('/declarations/{declarationId}/pushToES').onCreate(
    async (snapshot, context) => {
      console.log("Déclencheur lancé : onPushToEs")
      //const declarationId = context.params.declarationId
      const declarationRef = snapshot.ref.parent
      let declaration
      await declarationRef.once('value').then(function (dataSnapshot) { declaration = dataSnapshot.val() })
      //console.log('declaration parent de pushToEs', declaration)
      return new Promise((resolve, reject) => {
        if (declaration.declarationType === 'lost')
          updateMatchingList(admin, { declaration })
            .then(res => resolve(res))
            .catch(error => reject(error))
        else
          updateMatchedLostDeclarationList(admin, { declaration })
            .then(res => resolve(res))
            .catch(error => reject(error))
      })
    })

exports.addMessage = functions.https.onCall((data, context) => {
  return data
});


//Admin et esMatchingList
exports.onToNotificate = functions.database.ref('/declarations/{declarationId}/adminMatchingList/{matchId}/notificationList/{notificationId}')
  .onCreate(async (snapshot, context) => await _onToNotificate(snapshot, context));

async function _onToNotificate(snapshot, context) {
  const notificationDetail = snapshot.val();
  const { userId } = notificationDetail
  const title = `Nous avons trouvé votre objet perdu le ${notificationDetail.eventStringDate}  à ${notificationDetail.eventZoneUserInput}.`
  const description = `Pour rappel vous nous avez indiqué "${notificationDetail.userDescription}"`
  const image = notificationDetail.imageURL

  //console.log('dans _onToNotificate')
  try{
  return await sendNotification(admin, { userId, title, description, image })
    .then(res => Promise.resolve(res))
    .catch(error => Promise.reject(error))
  }catch(e)
  {console.log('dans _onToNotificate, erreur',e)}
}

exports.setThumbnail = functions.database.ref('/declarations/{declarationId}/imageURL')
  .onWrite(async (change, context) => {

    // Exit when the data is deleted.
    if (!change.after.exists()) {
      return null;
    }

    admin.database().ref(`images/${context.params.declarationId}`)
    .once('value').
    then(async dataSnapshot => {
      const {thumbnail} = dataSnapshot.val() 
      if (thumbnail)
          //await admin.database().ref(`/declarations/${declarationId}/thumbnailURL`).set(thumbnail)
          await change.after.ref.parent.child('thumbnailURL').set(thumbnail)
      })
      return null
  });

exports.generateThumbnail = functions.storage.object().onFinalize(async (object) => {
  'use strict';
  const mkdirp = require('mkdirp');
  const spawn = require('child-process-promise').spawn;
  const path = require('path');
  const os = require('os');
  const fs = require('fs');

  // Max height and width of the thumbnail in pixels.
  const THUMB_MAX_HEIGHT = 400;
  const THUMB_MAX_WIDTH = 400;
  // Thumbnail prefix added to file names.
  const THUMB_PREFIX = 'thumb_';

  /**
   * When an image is uploaded in the Storage bucket We generate a thumbnail automatically using
   * ImageMagick.
   * After the thumbnail has been generated and uploaded to Cloud Storage,
   * we write the public URL to the Firebase Realtime Database.
   */

  // File and directory paths.


  const filePath = object.name;
  const contentType = object.contentType; // This is the image MIME type
  const sourceMetadata = object.metadata || {}; // This is the image MIME type
  const fileDir = path.dirname(filePath);
  const fileName = path.basename(filePath);
  const thumbFilePath = path.normalize(path.join(fileDir, `${THUMB_PREFIX}${fileName}`));
  const tempLocalFile = path.join(os.tmpdir(), filePath);
  const tempLocalDir = path.dirname(tempLocalFile);
  const tempLocalThumbFile = path.join(os.tmpdir(), thumbFilePath);

  // Exit if this is triggered on a file that is not an image.
  if (!contentType.startsWith('image/')) {
    return console.log('This is not an image.');
  }

  // Exit if the image is already a thumbnail.
  if (fileName.startsWith(THUMB_PREFIX)) {
    return console.log('Already a Thumbnail.');
  }

  // Cloud Storage files.
  const bucket = admin.storage().bucket(object.bucket);
  const file = bucket.file(filePath);
  const thumbFile = bucket.file(thumbFilePath);
  const metadata = {
    contentType: contentType,
    customMetadata :sourceMetadata
    // To enable Client-side caching you can set the Cache-Control headers here. Uncomment below.
    // 'Cache-Control': 'public,max-age=3600',
  };

  // Create the temp directory where the storage file will be downloaded.
  await mkdirp(tempLocalDir)
  // Download file from bucket.
  await file.download({ destination: tempLocalFile });
  console.log('The file has been downloaded to', tempLocalFile);
  // Generate a thumbnail using ImageMagick.
  await spawn('convert', [tempLocalFile, '-thumbnail', `${THUMB_MAX_WIDTH}x${THUMB_MAX_HEIGHT}>`, tempLocalThumbFile], { capture: ['stdout', 'stderr'] });
  console.log('Thumbnail created at', tempLocalThumbFile);
  // Uploading the Thumbnail.
  await bucket.upload(tempLocalThumbFile, { destination: thumbFilePath, metadata: metadata });
  console.log('Thumbnail uploaded to Storage at', thumbFilePath);
  // Once the image has been uploaded delete the local files to free up disk space.
  fs.unlinkSync(tempLocalFile);
  fs.unlinkSync(tempLocalThumbFile);
  // Get the Signed URLs for the thumbnail and original image.
  const config = {
    action: 'read',
    expires: '03-01-2500',
  };
  const results = await Promise.all([
    thumbFile.getSignedUrl(config),
    file.getSignedUrl(config),
  ]);
  console.log('Got Signed URLs.');
  const thumbResult = results[0];
  const originalResult = results[1];
  const thumbFileUrl = thumbResult[0];
  const fileUrl = originalResult[0];

  try{
  // Add the URLs to the Database
  if (sourceMetadata.declarationId)
    await admin.database().ref(`images/${sourceMetadata.declarationId}`).set({ path: fileUrl, thumbnail: thumbFileUrl });

  if (sourceMetadata && sourceMetadata.declarationId){
    // Add the URLs to the Database
    await admin.database().ref(`declarations/${sourceMetadata.declarationId}/thumbnailURL`)
    .once(async snapshot=> {
      if (snapshot.exists()) 
          await snapshot.ref().set(thumbFileUrl);
    })
  }}
  catch(e){
    console.log("error pendant l'enregistremlent en base", sourceMetadata)
  }

  return console.log('Thumbnail URLs saved to database.');
});

exports.updateAllDeclarationMapping = functions.https.onCall((data, context) => {
  return new Promise(async (resolve, reject) => {
    await updateAllDeclarationMapping(admin, data)
      .then(res => resolve(res))
      .catch(error => reject(error))
  });
})


/*
 * Ici sont définis les méthodes pour les Users
 */
exports.onCreateUser = functions.auth.user().onCreate(async (user) => {

  const email = user.email; // The email of the user.
  const displayName = user.displayName; // The display name of the user.
  const uid = user.uid; // The id of the user.
  const isAdmin = false; // By default any user is admin.

  const userRef = admin.database().ref(`users/${uid}`)
  return await userRef.set({
    email: email,
    displayName: displayName,
    isAdmin: isAdmin,
   
  })
    .then(() => {
      //console.log('Add user in database successfull')
      return Promise.resolve('Add user in database successfull')
    })
    .catch(err =>
      Promise.reject('Failed to add user in database')
    )
});

exports.onDeleteUser = functions.auth.user().onDelete(async (user) => {
  const uid = user.uid; // The id of the user.

  const userRef = admin.database().ref(`users/${uid}`)
  return await userRef.remove()
    .then(() => {
      //console.log('Delete user in database successfull')
      return Promise.resolve('Delete user in database successfull')
    })
    .catch(err =>
      Promise.reject('Failed to delete user in database')
    )
});

exports.addFCMForUser = functions.region(process.env.REACT_APP_FUNCTIONS_REGION)
  .https.onCall(async (data, context) => {
    //console.log('data', data)
    const uid = data.uid // The id of the user from context

    var date = new Date
    var stringDate = (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear() + ' ' + date.toLocaleTimeString('fr-FR')

    const userRef = admin.database().ref(`users/${uid}/fcmIdList`)
    return await userRef.push({
      fcmId: data.fcmId,
      createdAt: stringDate
    })
      .then(() => {
        //console.log('FCM succesfully update for user with id : ' + uid)
        return Promise.resolve('FCM succesfully update for user with id : ' + uid)
      })
      .catch(err =>
        Promise.reject('Failed to update FCM')
      )
  });