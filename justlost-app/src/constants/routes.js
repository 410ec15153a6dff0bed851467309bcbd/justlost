export const LOGOUT = '/authentification/logout';
export const AUTHENTIFICATION = '/authentification';
export const EXEMPLES = '/samples';
export const HOME = '/home';
export const DECLARE_FOUND_GOOD = '/declarefoundedgood';
export const DECLARE_LOST_GOOD = '/declarelostgood';
export const HOME_NEW = '/home_new';
export const ADMIN_DECLARATION_LIST = '/admin/declarationlist';
export const QRCODE = '/qrcode';

