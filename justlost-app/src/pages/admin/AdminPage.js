import React, { Component, useState } from "react"
import SplitPane, { Pane } from 'react-split-pane';
import { withFirebase } from '../../components/Firebase';
import DeclarationList from '../../components/admin/DeclarationList'
import GoogleMaps from '../../components/googleMaps/GoogleMaps'
import GoodDetailCompare from '../../components/goods/GoodDetailCompare'
import Filters from '../../components/Filters/Filters'

import Categories from './Categories'
import './AdminPage.css'
class AdminPage extends Component {

  constructor(props) {
    super(props)
    this.state = {
      mapList: [],
      declarationList: [],
      declarationFoundMapList: new Map(),
      selectedCategory: "_all",
      selectedStatus:"_all",
      declaration: undefined,
      declarationCompared: undefined,
      positionToMarker: undefined,
      
    }
  }


  componentDidMount() {
    this.props.firebase.database().ref("declarations")
      //.orderByChild("status").equalTo("pending")
      .on('value', snapshot => {
        if (!snapshot.exists())
        return 
        const declarationList = Object.values(snapshot.val())
          .filter(declaration => declaration.declarationType === 'lost')
          .map((declaration, index) => { return { ...declaration, declarationMapNumber: index + 1 } })
        this.setState({ declarationList });
      });
  }

  componentWillUnmount = () => this.props.firebase.database().ref("declarations").off()

  getShortDeclarationList() {
    return this.state.declarationList.map(declaration => {
      return {
        declarationId: declaration.declarationId,
        status: declaration.status,
        categoryId: declaration.category.id,
        categoryCode: declaration.category.code,
        categoryLabel: declaration.category.label,
      }
    })
  }
  handleCategorySelected = (category) => {
    console.log('category', category)
    this.setState({ selectedCategory: category })
  }

  handleStatusFiltered = (status) => {
    this.setState({ selectedStatus: status })
  }

  getDeclarationListForCategory( ) {
    return this.getShortDeclarationList().filter(declaration => {
      return ((declaration.status === this.state.selectedStatus) || (this.state.selectedStatus === "_all"))})
  }

  getDeclarationListWithFilters( ) {
    return this.state.declarationList.filter(declaration => {
      return ((declaration.category.code === this.state.selectedCategory) || (this.state.selectedCategory === "_all")) && 
      ((declaration.status === this.state.selectedStatus) || (this.state.selectedStatus === "_all"))})
  }

  onMatchedSelected = async (declarationMatchedId, action, esNumber) => {
    let declarationMatched = undefined
    const declarationMatchedRef = this.props.firebase.database().ref(`declarations/${declarationMatchedId}`)
    await declarationMatchedRef.once("value")
      .then(snapshot => {
        if (snapshot.exists())
          declarationMatched = snapshot.val()
      })

    if (!declarationMatched)
      return

    
    if (action === 'viewPosition') {
      //On va se positionner sur l'objet trouvé, on ne pas utilsé de marker ici, parcequ'il sera créer en //
      const pos = {
        lat: declarationMatched.eventZone.userSelectionArea.latitude,
        lng: declarationMatched.eventZone.userSelectionArea.longitude
      }
      this.setState({ goToPosition: pos })
    }

    const declationCompared = {...declarationMatched, declarationMapNumber:esNumber}
    this.state.declarationFoundMapList.set(declarationMatched.declarationId, declationCompared)

    this.setState({ declarationCompared: declationCompared })
  }

  onDeclarationSelected = (declaration) => {
    this.setState({
      declaration,
      declarationCompared: undefined,
      positionToMarker: declaration.declarationMapNumber,
      declarationFoundMapList: new Map(),
      goToPosition:undefined
    })
  }

  handleDecision = async (data) => { // data à la forme :{ declarationId, matchId, decision, decisionOrigin } 
    console.log('Décision : ', data)

    var updateDecision = this.props.firebase.functions.httpsCallable('updateDecision')
    await updateDecision(data)
      .then(result => console.log(result))//this.setState({ calling: false, flashMessage: "Retour : " + result.data.text }))


  }
  render() {

    const displayedList = this.getDeclarationListWithFilters({category:true, status:true})
    const declarationFoundMapList = Array.from(this.state.declarationFoundMapList.values())
    //console.log('Array.from(this.state.declarationFoundMapList.values())', Array.from(this.state.declarationFoundMapList.values()))
    //console.log('declarationFoundMapList', declarationFoundMapList)


    const positionToMarker = this.state.positionToMarker
    let declarationCircles = []
    if (this.state.declaration)
      declarationCircles.push(this.state.declaration)
    if (this.state.declarationCompared)
      declarationCircles.push(this.state.declarationCompared)

    //console.log('declarationCircles', declarationCircles)
    return <div ><SplitPane className="m-2 p-2" split="vertical" minSize={"40%"}>

      <GoogleMaps
        declarationList={displayedList}
        declarationFoundMapList={declarationFoundMapList}
        handleMapDeclarationList={(mapList) => this.setState({ mapList: mapList.filter(declaration => declaration.declarationType === 'lost') })}
        declarationCircles={declarationCircles}
        zoom={5}
        positionToMarker={positionToMarker}
        goToPosition={this.state.goToPosition}
        resetPositionToMarker={() => this.setState({ positionToMarker: undefined })}
      />

      <SplitPane split="horizontal" minSize={"60%"}>
        <div style={{ overflow: "auto", width: "100%" }}>


          <div className="container-fluid mt-2">

          <div className="mv-2 mb-3 align-items-center  justify-content-center" id="categories" name="categories">
              <div className="row">
                <div className="col-12">
                  <span >Répartition par statut</span>
                </div>
              </div>
              <div className="row">
                <div className="col-12">
                  <Filters 
                  fieldFilter="status"
                  defaultSelection={this.state.selectedStatus}
                  filtersConfig={[{filter:"_all", label:"Tous"}, {filter:"pending", label:"Non retrouvé"}, {filter:"matched", label:"Trouvé"}, {filter:"cancelled", label:"Annulé"}, {filter:"closed", label:"Fermé"}]}
                  shortDeclarationList={this.getShortDeclarationList()} 
                  handleFilterSelected={(status) => this.handleStatusFiltered(status)} />
                </div>
              </div>
            </div>

            <div className="mv-2 mb-3 align-items-center  justify-content-center" id="categories" name="categories">
              <div className="row">
                <div className="col-12">
                  <span >Nombre d'objet perdu par catégorie</span>
                </div>
              </div>
              <div className="row">

                <div className="col-12">
                  <Filters 
                  fieldFilter="categoryCode"
                  defaultSelection={this.state.selectedStatus}
                  filtersConfig={[
                    {filter:"_all", label:"Tous"}, 
                    {filter:"toy", label:"Jouet"}, 
                    {filter:"electronic", label:"Electronique"}, 
                    {filter:"clothes", label:"Vêtement"}, 
                    {filter:"wallet", label:"Portefeuille"}, 
                    {filter:"jewelry", label:"Bijou"}, 
                    {filter:"keys", label:"Clés"}
                    ]}
                  shortDeclarationList={this.getDeclarationListForCategory()} 
                  handleFilterSelected={(filter) => this.handleCategorySelected(filter)} />
                </div>


              </div>
            </div>

            <DeclarationList
              documents={displayedList}
              onSelect={declaration => this.onDeclarationSelected(declaration)}
              onMatchedSelected={(declarationMatchedId, action, esNumber) => this.onMatchedSelected(declarationMatchedId, action, esNumber)}
              handleDecision={(decision, matchId) => this.handleDecision(decision, matchId)}
            ></DeclarationList></div>
        </div>
        <div>

          <SplitPane split="vertical" minSize={"50%"}>
            <div style={{ overflow: "hidden", width: "100%" }}>
              <div>
                {this.state.declaration && <GoodDetailCompare declaration={this.state.declaration} declarationCompared={this.state.declarationCompared} />}
              </div>
            </div>
            {this.state.declarationCompared && <GoodDetailCompare declaration={this.state.declarationCompared} declarationCompared={this.state.declaration} />}
            <div className="text-center d-flex justify-content-center align-middle">

              <span>
                Aucune correspondance de sélectionnée.
            </span>

            </div>
          </SplitPane>

        </div>
      </SplitPane>
    </SplitPane></div>
  }
}
export default withFirebase(AdminPage)