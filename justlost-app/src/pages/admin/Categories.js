import React, { Component } from "react"
import { withFirebase } from '../../components/Firebase';



class Categories extends Component {
    constructor(props) {
        super(props)
        this.state ={
            shortDeclarationList: props.shortDeclarationList,
            categories: {},
            seletedCategory:undefined
        }
        this.labels={}
    }

    getCategoryLabel(category){
        return this.labels[category]
    }

    UNSAFE_componentWillReceiveProps(newProps){
        const newCategories = {}
        const shortDeclarationList = newProps.shortDeclarationList
        shortDeclarationList.forEach(shortDeclaration => {
            const curValue = newCategories[shortDeclaration.categoryCode]
            newCategories[shortDeclaration.categoryCode] = curValue ? curValue + 1 : 1
            this.labels[shortDeclaration.categoryCode]=shortDeclaration.categoryLabel
        });
        this.setState({ categories: newCategories })
    }

    handleCategorySelected = (category) => {
        console.log('seletedCategory', category)
        this.setState({seletedCategory:category}, this.props.handleCategorySelected(category))
    }

    render() {

        return (
            <div className="">
                <button key = "_all" type="button" className={this.state.seletedCategory && this.state.seletedCategory==="all"?"btn btn-primary mr-2":"btn btn-outline-primary mr-2"} onClick={() => this.handleCategorySelected("_all")}>
                   Toutes catégories <span className="badge badge-light">{Object.entries(this.state.categories).reduce((cumul, [category, count])=>{return cumul+count},0)}</span>
                </button>

                {Object.entries(this.state.categories).map(([category, count]) => 
                <button  key = {category} type="button" className={this.state.seletedCategory && this.state.seletedCategory===category?"btn btn-primary mr-2":"btn btn-outline-primary mr-2"} onClick={() => this.handleCategorySelected(category)}>
                   {this.getCategoryLabel(category)} <span className="badge badge-light">{count}</span>
                </button>)
                }
            </div>
        )
    }

}
export default withFirebase(Categories)