import React from 'react'
import './style.css';


export default class HomePage extends React.Component {
    render() {
        return (<div> <div>
            <div className="container">
                <header>
                    <div className="header-section">
                        <div className="row">
                            <div className="col-md-12">
                                <nav className="navbar navbar-expand-lg custom-navbar">
                                    <a className="navbar-brand logo" href="#" img src="logo.png">Just Lost</a>
                                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                        <span className="navbar-toggler-icon"><i className="fa fa-bars"></i></span>
                                    </button>

                                    <div className="collapse navbar-collapse menu" id="navbarSupportedContent">
                                        <ul className="navbar-nav ml-auto">
                                            <li className="nav-item active">
                                                <a className="nav-link" href="#">Home <span className="sr-only">(current)</span></a>
                                            </li>
                                            <li className="nav-item">
                                                <a className="nav-link" href="#">Sigin</a>
                                            </li>
                                            <li className="nav-item">
                                                <a className="nav-link" href="#">Register</a>
                                            </li>
                                            <li className="nav-item">
                                                <a className="nav-link" href="#">Contact</a>
                                            </li>
                                            <li className="nav-item">
                                                <a className="nav-link" href="#">About</a>
                                            </li>
                                        </ul>
                                    </div>
                                </nav>
                            </div>
                        </div>
                    </div>
                </header>
            </div>
            <div className="hero-section">
                <div className="row">
                    <div className="col-md-12 display">
                        <div className="hero-content text-center text-white">
                            <div className="slogan" color="white">
                                <h1>Retrouvez vos objets perdus et aidez vos vos voisins à retrouver les leurs</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <section className="content-section">
                <div className="row">
                    <div className="col-md-12">
                        <input type="button" className="find" value="J'ai trouvé" style={{ color: 'green' }} />
                        <input type="button" className="loose" value="J'ai perdu" />
                        <div className="content">
                        </div>
                    </div>
                </div>
            </section>
            <div className="doudou">
                <img src="doudou.jpeg"/>
            </div>
            <div className="text-find">
        <h2> Comment faire si j'ai trouvé un objet ? </h2>
        <ul className="puce"> 
            <h5>
            <li>Postez une photo de l'objet trouvé</li>
            <li>Ajouter un commentaire</li>
            <li>Préciser la ville</li>
            <li>Notre algorithme génère automatique des mots clés <br/> en l'associant à l'objet trouvé</li>
            <li>Nous vous mettons en contact avec les personnes <br/> ayant perdus le même objet</li>
            <li>Faites un heureux !</li>
            </h5>
        </ul>
    </div>
    <div className="text-loose">
        <h2> Comment faire si j'ai perdu un objet ? </h2>
        <ul className="puce"> 
            <h5>
            <li>Créez un compte Just Lost</li>
            <li>Poster une photo de votre objet perdu</li>
            <li>Préciser la ville</li>
            <li>Notre algorithme génère automatique des mots clés <br/> en l'associant à votre objet perdu</li>
            <li>Nous vous mettons en contact avec les personnes <br/> ayant trouvé le même objet</li>
            </h5>
        </ul>
    </div>  
    <div className="container-fluid" className="footer">
    <div className="row footer-top">
        <div className="col-sm-4 text-center">
            <h4 className="ft-text-title">Media Name</h4>
            <h6 className="ft-desp">Company Name 
                <br/>Country Name
            </h6>
            <h4 className="details">
                <a className="contact" href="tel:+977-1-4107223">
                    <i className="fa fa-phone" aria-hidden="true"></i> +977-000000</a>
                </h4>
            </div>
            <div className="col-sm-4 text-center border-left">
                <h4 className="ft-text-title">Our Team</h4>
                <div className="address-member">
                    <p className="member">
                        <b>Director</b> : 
                    </p>
                    <p className="member">
                        <b>Editor</b> : 
                    </p>
                    <p className="member">
                        <b>Reporter</b> : 
                    </p>
                    <p className="member">
                        <b>Reporter</b> : 
                    </p>
               </div>
           </div>
           <div className="col-sm-4 col-xs-12 text-center border-left">
            <h4 className="ft-text-title">About</h4>
            <div className="pspt-dtls">
                <a href="#" className="about">About</a>
                <a href="#" className="team">Team</a>
                <a href="#" className="advertise">Advertise</a>
                <br/><br/><br/><br/><br/><br/>
            </div>
        </div>
    </div>
	<div className="row ft-copyright pt-2 pb-2" style={{paddingLeft: "25px;"}}>
		<div className="col-sm-4 text-pp-crt">cpoyright 2018 All  Rights Reserved</div>
		<div className="col-sm-4 text-pp-crt-rg">Department of Information Reg No :</div>
		<div className="col-sm-4 developer">
			<a href="https://topline-tech.com" target="_blank" className="text-pp-crt">By : <b>T</b>op<b>L</b>ine</a>
		</div>
	</div>
</div>
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </div>
                </div>
                
            )
    }
} 
