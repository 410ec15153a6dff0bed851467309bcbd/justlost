import React from 'react';
import { getExifGPS } from '../../../helpers/getExifGPS'


export default class UploadImage extends React.Component {
    constructor(props) {
        //Obligatoire quand on surcharge le constructeur
        super(props)
        this.state = { percentUpdload: 0 }
        //Je récupère la db et la stocke au niveau de mon object pour une faciliter d'écriture
        this.storage = props.firebase.storage;

        this.uploadImage = this.uploadImage.bind(this)
        this.handleFileSelected = this.handleFileSelected.bind(this)
    }

    async uploadImage(event) {
        var metadata={}
        event.preventDefault()
        // Create a root reference
        var storageRef = this.storage.ref();
        var file = this.state.fileToUpdate

        const gps = await getExifGPS(file)
        if (gps){
            this.setState({gps})
            metadata = {
                customMetadata: {
                    gps_latitude: gps.latitude,
                    gps_longitude: gps.longitude
                }
            };
        }

        var imageRef = storageRef.child(file.name);

        //Ajouter le test du type if (!contentType.startsWith('image/')) {

        const uploadTask = imageRef.put(file, metadata)
        uploadTask.on('state_changed', (snapshot) => {
            // Observe state change events such as progress, pause, and resume
            // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
            var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            this.setState({ percentUpdload: progress })
        }, function (error) {
            // Handle unsuccessful uploads
            this.setState({ error })
        }, () => {
            // Handle successful uploads on complete
            console.log('imageRef', imageRef)    
            imageRef.getDownloadURL().then(downloadURL => {
                this.setState({ downloadURL })
            });

        });


    }

    handleFileSelected(e) {
        let file = e.target.files[0];
        this.setState({
            [e.target.id]: file
        })
    }

    render() {
        return(
        <div style={{width:300}}>
            <div className="row" >
                <div className="col mb-3">
                    <input
                        type="file"
                        id="fileToUpdate"
                        accept=".jpg, .png, .heif, .heic"
                        onChange={this.handleFileSelected}
                    />
                </div>
            </div>
            { this.state.fileToUpdate && <div className="row">
                <div className="col mb-3 ">
                    <a href="." onClick={this.uploadImage}>Télécharger</a>
                </div>
            </div>
            }
            { this.state.fileToUpdate && 
                <div className="row">
                    <div className="col mb-3">
                    { this.state.gps &&  <span>GPS : {this.state.gps.latitude}, {this.state.gps.longitude}</span>}
                    { !this.state.gps &&  <span> Pas de EXIF pour obtenir le GPS </span>}
                    </div>
                 </div>
            }
            


            <div className="row">
                <div className="col mb-3">
                    <div className="progress">
                        <div className="progress-bar danger" role="progressbar" style={{ width: this.state.percentUpdload + '%' }} aria-valuemin="0" aria-valuemax="100">{Math.round(this.state.percentUpdload,1)}</div>
                    </div>
                </div>
            </div>
            { this.state.downloadURL && <a href={this.state.downloadURL}>Lien vers le fichier</a> }
            { this.state.downloadURL && <img alt="" className="rounded img-thumbnail" src={this.state.downloadURL}></img>}
        </div>)
    }
}