import React from 'react';

//import { reIndexAllDocumentInElastic } from '../../../services/elastic/indexation'
/**
 * Exemple ave ElasticSearch afin d'indexer des documents et d'y faire des recheches
  * 
 */
export default class ElasticSearch extends React.Component {

    constructor(props) {
        //Obligatoire quand on surcharge le constructeur
        super(props)

        //Je récupère la db et la stocke au niveau de mon object pour une faciliter d'écriture
        this.database = props.firebase.database();
        this.functions = props.firebase.functions;

        const collection = props.collection || 'declarations'
        this.state = {
            indexation: false,
            deleting:false,
            collection: collection,
            esIndex: collection,
            flashMessage: undefined
        }

    }

    /**
     * Quand le composant est monté
     * on récupère la liste et on s'abonne à tout changement sur la Db.
     * c'est la méthode 'on' qui permet de d'abonner, il est possible de récupérer une fois et de ne pas s'abonner 
     * alors il faut utiliser 'once'.
     * Sur l'abonnement on met à jour le state du composant ce qui provoque son rafraichissement automatique dès que qql change sur la bdd.
     * Attention, NE PAS OUBLIER DE SE DESABONNER. on le fait quand le composant est démonté 'componentWillUnmount'
     */

    componentDidMount() {
        this.setState({});
    }
    /**
     */

    deleteIndex = (e)=>{
        e.preventDefault()
        const { esIndex } = this.state
        this.setState({ deleting: true, flashMessage: undefined })
        
        var indexAllInES =  this.functions.httpsCallable('removeIndexInES');
        indexAllInES({ esIndex: esIndex })
        .then(result => {
            console.log(result)
            // ...
            this.setState({ deleting: false })
        })
        .catch(error =>  this.setState({ deleting: false, flashMessage: undefined, error:error.flashMessage}))
    }

    reindex = (e) => {

        //Ne fonctionne pas. Pb avec l'appel functions

        e.preventDefault()
        const { esIndex, collection } = this.state
        this.setState({ indexation: true, flashMessage: undefined })
        
        var indexAllInES =  this.functions.httpsCallable('indexAllInES');
        indexAllInES({ collection, esIndex })
        .then(result => {
            console.log(result)
            // ...
            this.setState({ indexation: false })
        })
        .catch(error =>  this.setState({ indexation: false, flashMessage: undefined, error:error.flashMessage}))

        

    }


    //****************************************************************************************** */


    /**
     * Gestion des handles des éléments du DOM (input, boutton)
     */
    handleChangeKeyItem = (e) => this.setState({ flashMessage: undefined, [[e.target.name]]: e.target.value })
    

    /** Le render
     * 
     */

    render() {
        return (
            <div style={{ width: 300 }} >

                {this.state.flashMessage && <div className="alert alert-success" role="alert">
                    {this.state.flashMessage}
                </div>}
                {this.state.error && <div className="alert alert-danger" role="alert">
                    {this.state.error}
                </div>}
                {this.state.indexation && <div className="alert alert-info" role="alert">
                    "Indexation en cours"
                </div>}

                <div className="form-group"  >
                    <label className="mb-0" htmlFor="collection">Firebase collection</label>
                    <input type="text" className="form-control" id="collection" name="collection" placeholder="Collection Name" onChange={this.handleChangeKeyItem} value={this.state.collection} />
                </div>
                <div className="form-group" >
                    <label className="mb-0" htmlFor="collection">Elastic Index</label>
                    <input type="text" className="form-control" id="esIndex" name="esIndex" placeholder="Elastic Index" onChange={this.handleChangeKeyItem} value={this.state.esIndex} />
                </div>

                <div className="row">
                    <div className="col mb-3">
                    {!this.state.indexation  &&
                        <button className="btn btn-success" onClick={this.reindex}>Ré-indexer</button>
                    }
                    {this.state.indexation  &&
                        <div className="spinner-border text-primary" role="status">
                            <span className="sr-only">Loading...</span>
                        </div>
                    }
                    </div>
                </div>
                <div className="row">
                    <div className="col mb-3">
                    {!this.state.deleting  &&
                        <button className="btn btn-success" onClick={this.deleteIndex}>Supprimer l'index</button>
                    }
                    {this.state.deleting  &&
                        <div className="spinner-border text-primary" role="status">
                            <span className="sr-only">Deleting...</span>
                        </div>
                    }
                    </div>
                </div>                
            </div>
        );
    }
}