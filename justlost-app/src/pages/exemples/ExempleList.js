import React from 'react';
import CRUD from './database/CRUD'
import UploadImage from './storage/UploadImage'
import ElasticSearch from './es/ElasticSearch'
import MapApiMisc from './map/mapApiMisc'
import AddFCM from './functions/addFCM'
import AddMessage from './functions/addMessage'
import createConfig from './functions/createConfig'
import Tags from '../../components/goods/tags/Tags'
import MatchingAll from './functions/MatchingAll'


import { FirebaseContext } from '../../components/Firebase';


import './exemple.css';

/**
 * Affiche une liste d'exemples de code React avec Firebase pour le projet JustLost
 * Remplir le tableau exemples avec le format suivant format : 
 * { title:<string>, component:<React.Component | string>, props: <object> }
 */

const exemples = [
    { title: 'Mapping / Indexation / Recherche', title2:'Elastic Search', component: ElasticSearch },
    { title: 'MatchingAll', title2:'Rematch all lost declarations', component: MatchingAll, props:{} },
    { title: 'Storage', title2:'Upload Image', component: UploadImage, props:{} },
    { title: 'CRUD', title2:'Realtime Database', component: CRUD, props: { collection: 'students' } },
    { title: 'Map Api', title2:'OpenStreetMap / Leaflet', component: MapApiMisc, props: {} },
    { title: 'Firebase Functions', title2:'Ajouter un fcmId au compte connecté', component: AddFCM, props: {} },
    { title: 'Firebase Functions', title2:'Tests', component: AddMessage, props: {} },
    { title: 'Firebase Functions', title2:'Création de la configuration', component: createConfig, props: {} },
    { title: 'Tags', title2:'Gestion des tags selon le texte saisie', component: Tags, props: {text:"J'ai perdu ma veste et veste et veste Salomon Bonatti WP rouge et bordeaux avec son zip jaune pour homme"}, width:'500px' },    
]

export default class ExempleList extends React.Component {
    render() {
        return (
            /**
             * FirebaseContext.Consumer permet de récupérer le context de Firebase
             * Attention : La synthaxe est <MonContext.Consumer> { valueRecuperer => bloc de code}
             * dans l'exemple en dessous : <FirebaseContext.Consumer> { firebase => ... <exemple.component firebase={firebase}/> }
             */

            /** 
            * Ici pour boucler, au lieu d'utiliser un for_each, 
            * on utilise la methode "map" qui existe sur les tableaux [item1, item2]
            par défaut, on récupère comem premier argument :
            - le contenu pour un tableau : item1, ensuite item2 ...

            ça peut aussi s'utiliser sur un object en passant par example par Object.entries()
            https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Object/entries

            le deuxième paramètre du map est opétionnel, c'est l'index de l'élément.
            https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Map
                */

            <FirebaseContext.Consumer>
                {firebase =>
                    <div className="">
                    {
                        
                            /**
                             Il est obligatoire de mettre la prop key sur les éléments d'une liste.
                                Cette clé ne doit pas changer. Elle permet à react de mettre à jour uniquement certaines lignes de la liste.
                              */
                            exemples.map(exemple => (
                                <div id="exemple"><h2>{exemple.title2}</h2><div key={exemple.title} >
                                    <h1>{exemple.title}</h1>
                                    <exemple.component firebase={firebase} {...exemple.props} />
                                </div></div>

                            ))
                    }
                    </div>
                }
            </FirebaseContext.Consumer>
        );
    }
}