import React, { Component } from "react"
import { Map, TileLayer, Circle } from "react-leaflet"
import axios from "axios"
import "./personalStyle.css"

class mapApiMisc extends Component {

    constructor() {
        super()

        this.state = {
            info1: "",              // Basic object information
            info2: "",              //
            info3: "",              //

            lostAreaX: 0,           // Coords of the lost area
            lostAreaY: 0,           // Also used for the preview

            search: "",             // The input search
            searchResult: [],       // Will be filled with api result, as an array of json
            searchResultActive: ""  // Value of the current selection
        }
        this.onChange = this.onChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)

        this.onSearchLocation = this.onSearchLocation.bind(this)

        this.listResult = this.listResult.bind(this)
        this.onChangeChooseResult = this.onChangeChooseResult.bind(this)
    }










    /* change/submit basic function */
    onChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }
    onSubmit(event) {
        event.preventDefault()
        const item = {
            info1: this.state.info1,
            info2: this.state.info2,
            info3: this.state.info3,
            lostArea: [
                {
                    x: this.state.lostAreaX,
                    y: this.state.lostAreaY
                }
            ]
        }

        /* Store the new item in the database */

        alert("the item is : [" + item.info1 + ", " + item.info2 + ", " + item.info3 + ", " + item.lostArea[0].x + ", " + item.lostArea[0].y + "]")
    }



















        
    /* search section function */
    onSearchLocation() {
        let search = this.state.search;
        axios
            .get(
                "https://nominatim.openstreetmap.org/search?q=" + search + "&format=json"
            )
            .then((res) => {
                if (res.data.length === 0) {
                    console.log("no result for " + search)
                } else {
                    let arrOfResponse = res.data.map(currentResponse => {
                        return (
                            {
                                name: currentResponse.display_name,
                                osmId: currentResponse.osm_id,
                                x: currentResponse.lat,
                                y: currentResponse.lon
                            }
                        )
                    })
                    this.setState({
                        searchResult: arrOfResponse,      // Set the array of result in state
                        lostAreaX: arrOfResponse[0].x,    // From the array of result, set the preview
                        lostAreaY: arrOfResponse[0].y     // with the coords of the first result
                    })
                }
            })
            .catch((err) => {
                console.log(err)
            })
    }






















    /* select/option tag function */
    listResult() {
        // Fill the select with formated option tag
        return this.state.searchResult.map(currentResult => {
            let idXY = currentResult.osmId + "&" + currentResult.x + "&" + currentResult.y
            return (
                <option
                    key={currentResult.osmId}
                    value={idXY}
                >
                    {currentResult.name}
                </option>
            )
        })
    }

    onChangeChooseResult(event) {
        let arrOfParams = event.target.value.split("&")
        let osmId = arrOfParams[0]
        let coordX = parseFloat(arrOfParams[1], 10)
        let coordY = parseFloat(arrOfParams[2], 10)
        this.setState({
            searchResultActive: event.target.value,
            lostAreaX: coordX,
            lostAreaY: coordY
        })
    }






















    render() {

        const searchResultDisabled = (
            <select
                className="form-control"
                disabled={true}
            >
                <option>You must type a location</option>
            </select>
        )

        const searchResult = (
            <select
                className="form-control"
                value={this.state.searchResultActive}
                onChange={this.onChangeChooseResult}
            >
                {this.listResult()}
            </select>
        )

        const searchResultMap = (
            <Map
                center={[
                    this.state.lostAreaX,
                    this.state.lostAreaY
                ]}
                zoom={10}
                scrollWheelZoom={false}
            >
                <Circle
                    center={[
                        this.state.lostAreaX,
                        this.state.lostAreaY
                    ]}
                    radius={1000}
                >
                    <TileLayer
                        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    />
                </Circle>
            </Map>
        )



        return (
            <div className="container">

                {/* Leaflet need its style and script */}
                <link
                    rel="stylesheet"
                    href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
                    integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
                    crossorigin=""
                />
                <script
                    src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
                    integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
                    crossorigin=""
                ></script>
                {/* End of needed things */}

                <div className="row">
                    <div className="col">

                        <form noValidate onSubmit={this.onSubmit}>

                            <h1 className="h3">
                                Create an item
                            </h1>







                            {/* Object information */}
                            <div className="form-group">
                                <label htmlFor="info1">
                                    info1
                                </label>
                                <input
                                    className="form-control"
                                    name="info1"
                                    type="text"
                                    placeholder="info1"
                                    value={this.state.info1}
                                    onChange={this.onChange}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="info2">
                                    info2
                                </label>
                                <input
                                    className="form-control"
                                    name="info2"
                                    type="text"
                                    placeholder="info2"
                                    value={this.state.info2}
                                    onChange={this.onChange}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="info3">
                                    info3
                                </label>
                                <input
                                    className="form-control"
                                    name="info3"
                                    type="text"
                                    placeholder="info3"
                                    value={this.state.info3}
                                    onChange={this.onChange}
                                />
                            </div>







                            {/* Map input */}
                            <div className="row">




                                {/* search input */}
                                <div className="col-6">
                                    <div className="form-group">
                                        <label htmlFor="search">
                                            search
                                        </label>
                                        <input
                                            className="form-control"
                                            name="search"
                                            type="text"
                                            placeholder="search"
                                            value={this.state.search}
                                            onChange={this.onChange}
                                        />
                                    </div>
                                </div>

                                {/* sub search */}
                                <div className="col-6">
                                    <div className="form-group">
                                        <label htmlFor="submitSearch">
                                            &nbsp;
                                        </label>
                                        <button
                                            type="button"
                                            className="btn btn-primary btn-block"
                                            onClick={this.onSearchLocation}
                                        >
                                            Search
                                        </button>
                                    </div>
                                </div>





                                {/* result search */}
                                <div className="col">
                                    <div className="form-group">
                                        <label htmlFor="search result">
                                            &nbsp;
                                        </label>

                                        
                                        {
                                            this.state.searchResult.length === 0
                                                ?
                                                searchResultDisabled
                                                :
                                                searchResult
                                        }

                                    </div>
                                </div>
                            </div>

                            {/* Map preview */}
                            <div className="row">
                                <div className="col">
                                    {
                                        this.state.searchResult.length === 0
                                            ?
                                            ""
                                            :
                                            searchResultMap
                                    }
                                </div>
                            </div>









                            <button
                                type="submit"
                                className="btn btn-lg btn-primary btn-block"
                            >
                                Create item
                            </button>





                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default mapApiMisc