import React from 'react';
/**
 * Exemple de CRUD avec la Database Realtime de firebase. 
 * Attention, ce n'est pas FireStore qui est un autre modéle de base et donc une synthaxe autre pour lire et écrore dedans
 * 
 */
export default class CRUD extends React.Component {

    constructor(props) {
        //Obligatoire quand on surcharge le constructeur
        super(props)

        //Je récupère la db et la stocke au niveau de mon object pour une faciliter d'écriture
        this.database = props.firebase.database();

        this.state = {
            loading: true,
            collection: props.collection || 'students',
            documents: [],
            startCreating: false
        }

        //Les méthodes bindées pour avoir le bon this.
        this.handleClickToAddItem = this.handleClickToAddItem.bind(this)
        this.handleClickToValidateAddItem = this.handleClickToValidateAddItem.bind(this)
        this.handleChangeKeyItem = this.handleChangeKeyItem.bind(this)
        this.handleClickRemoveItem = this.handleClickRemoveItem.bind(this)
    }

    /**
     * Quand le composant est monté
     * on récupère la liste et on s'abonne à tout changement sur la Db.
     * c'est la méthode 'on' qui permet de d'abonner, il est possible de récupérer une fois et de ne pas s'abonner 
     * alors il faut utiliser 'once'.
     * Sur l'abonnement on met à jour le state du composant ce qui provoque son rafraichissement automatique dès que qql change sur la bdd.
     * Attention, NE PAS OUBLIER DE SE DESABONNER. on le fait quand le composant est démonté 'componentWillUnmount'
     */

    componentDidMount() {
        this.setState({ loading: true });

        this.getAll()
            .on('value', snapshot => {

                let documents = []
                const documentsObject = snapshot.val();

                if (documentsObject) {
                    //console.log('documentsObject', documentsObject)
                    documents = Object.keys(documentsObject).map(key => ({
                        ...documentsObject[key],
                        id: key,
                    }))
                }

                this.setState({
                    documents,
                    loading: false,
                });
            });
    }

    componentWillUnmount = () => this.removeEventListener()

    /**
     * Firebase Database méthodes
     * ici l'on a toutes les méthodes pour accéder à la DBB RealTime de Firebase
     * c'est principalement avec this.database.ref(Chemin/Element), voir la doc Firebase
     * L'écriture ici correspond à des attributs de la classe CRUD.
     * on associe une fonction anonyme à un attribut de la classe
     */

    getAll = () => this.database.ref(this.state.collection);
    /**
    getAll = function () { this.database.ref(this.state.collection);}
    getAll = function () { return this.database.ref(this.state.collection);}
    getAll = (param1, param2) => {a=2; return this.database.ref(this.state.collection)};
    getAll = (param1, param2) =>  this.database.ref(this.state.collection);
    getAll(){
        return this.database.ref(this.state.collection);
    } */

    getOne = (id) => this.database.ref(`${this.state.collection}/${id}`);
    addDocument = (document) => {
        var collectionRef = this.database.ref(this.state.collection);
        var newDocummentRef = collectionRef.push(); // Ajoute un item 
        newDocummentRef.set({
            'title': document.title,
            'content': document
        })

    }
    removeOne = id => id && this.database.ref(`${this.state.collection}/${id}`).remove();
    removeEventListener = () => this.database.ref(this.state.collection).off();

    //****************************************************************************************** */


    /**
     * Gestion des handles des éléments du DOM (input, boutton)
     */
    handleNewItemChange = event => this.setState({ [event.target.name]: event.target.value });
    handleClickToAddItem = () => this.setState({ startCreating: true })
    handleChangeKeyItem = (e) => this.setState({ startCreating: true, [[e.target.name]]: e.target.value })
    handleClickRemoveItem = (id) => this.removeOne(id)
    handleClickToValidateAddItem() {
        this.setState({ startCreating: false, creating: true })
        this.addDocument({ title: this.state.itemToAdd })
    }


    /** Le render
     * 
     */

    render() {
        const btnParam = this.state.startCreating ? ["btn btn-success", "Validate"] : ["btn btn-primary", "Ajouter"]
        const deleteButton = id => <span className="table-remove">
            <button type="button" className="btn btn-danger btn-rounded btn-sm my-0" onClick={() => this.handleClickRemoveItem(id)}>Remove</button></span>

        return (
            <div>
                <div className="form-group" >
                    <label className="mb-0" htmlFor="collection">Collection</label>
                    <input type="text" className="form-control" id="collection" name="collection" placeholder="Collection Name" onChange={this.handleChangeKeyItem} value={this.state.collection} />
                </div>

                <div className="input-group mb-3">
                    <input type="text" id="itemToAdd" name="itemToAdd" onChange={this.handleChangeKeyItem} className="form-control" placeholder="Item to add" aria-describedby="basic-addon2" />
                    <div className="input-group-append">
                        {}
                        <button className={btnParam[0]} type="button" onClick={this.handleClickToValidateAddItem}>{btnParam[1]}</button>
                    </div>
                </div>
                <div className="mt-4 mb-4">
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Title</th>
                                <th scope="col">Content</th>
                                <th scope="col">Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.documents
                                    .map((document, index) => {
                                        //console.log(document);
                                        return <tr key={document.id}><td>{index}</td><td onInput={e => console.log('Text inside div', e.currentTarget.textContent)}>{document.title} </td><td>{JSON.stringify(document.content)} </td><td>{deleteButton(document.id)}</td></tr>
                                    }
                                    )
                            }
                        </tbody>
                    </table>


                </div>
            </div>
        );
    }
}