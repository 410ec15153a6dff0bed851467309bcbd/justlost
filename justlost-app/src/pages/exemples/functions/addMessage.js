import React from 'react';

//import { reIndexAllDocumentInElastic } from '../../../services/elastic/calling'
/**
 * Exemple ave ElasticSearch afin d'indexer des documents et d'y faire des recheches
  * 
 */
export default class addMessage extends React.Component {

    constructor(props) {
        //Obligatoire quand on surcharge le constructeur
        super(props)

        //Je récupère la db et la stocke au niveau de mon object pour une faciliter d'écriture
        this.database = props.firebase.database();
        this.functions = props.firebase.functions;

        this.state = {
            calling: false,
            flashMessage: undefined
        }

    }

    componentDidMount() {
        this.setState({});
    }
    /**
     */


    callFunctionTest = (e) => {

        e.preventDefault()
        this.setState({ calling: true, flashMessage: undefined })

        console.log('this.state.testInput', typeof this.state.testInput, this.state.testInput)
        
        console.log('auth', this.props.firebase.auth)
        console.log('uid', this.props.firebase.auth.currentUser.uid)
    
        var addMessage = this.functions.httpsCallable('addMessage')
        addMessage({text: this.state.testInput})
        .then(result=>  this.setState({ calling: false, flashMessage: "Hello Ok : " + result.data.text }))

        return
    }

    

    //****************************************************************************************** */


    /**
     * Gestion des handles des éléments du DOM (input, boutton)
     */
    handleChangeKeyItem = (e) => this.setState({ flashMessage: undefined, [[e.target.name]]: e.target.value })
    

    /** Le render
     * 
     */

    render() {
        return (
            <div style={{ width: 300 }} >

                {this.state.flashMessage && <div className="alert alert-success" role="alert">
                    {this.state.flashMessage}
                </div>}
                {this.state.error && <div className="alert alert-danger" role="alert">
                    {this.state.error}
                </div>}
                {this.state.calling && <div className="alert alert-info" role="alert">
                    "calling en cours"
                </div>}

                <div className="form-group" >
                    <label className="mb-0" htmlFor="collection">Test Input</label>
                    <input type="text" className="form-control" id="testInput" name="testInput" placeholder="Elastic Index" onChange={this.handleChangeKeyItem} value={this.state.esIndex} />
                </div>

                <div className="row">
                    <div className="col mb-3">
                    {!this.state.calling  &&
                        <button className="btn btn-success" onClick={this.callFunctionTest}>Test</button>
                    }
                    {this.state.calling  &&
                        <div className="spinner-border text-primary" role="status">
                            <span className="sr-only">Loading...</span>
                        </div>
                    }
                    </div>
                </div>
            </div>
        );
    }
}