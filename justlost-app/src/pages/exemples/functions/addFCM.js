import React from 'react';

//import { reIndexAllDocumentInElastic } from '../../../services/elastic/calling'
/**
 * Exemple ave ElasticSearch afin d'indexer des documents et d'y faire des recheches
  * 
 */
export default class addFCM extends React.Component {

    constructor(props) {
        //Obligatoire quand on surcharge le constructeur
        super(props)

        //Je récupère la db et la stocke au niveau de mon object pour une faciliter d'écriture
        this.database = props.firebase.database();
        this.functions = props.firebase.functions;

        this.state = {
            calling: false,
            flashMessage: undefined,
            uid: '',
            fcmId: ''
        }

    }

    componentWillMount() {
        this.setState({});
    }
    /**
     */


    callFunctionTest = (e) => {

        e.preventDefault()
        this.setState({ calling: true, flashMessage: undefined })
        
        var addFCMForUser = this.functions.httpsCallable('addFCMForUser')
        addFCMForUser({fcmId: this.state.fcmId, uid: this.state.uid })
        .then(()=>  this.setState({ calling: false, flashMessage: "FCM id bien ajouté pour " + this.state.uid }))

        return
    }

    

    //****************************************************************************************** */


    /**
     * Gestion des handles des éléments du DOM (input, boutton)
     */
    handleChangeKeyItem = (e) => this.setState({ flashMessage: undefined, [[e.target.name]]: e.target.value })
    
    handleClickId = (e) => this.setState({ uid: this.props.firebase.auth.currentUser.uid })

    /** Le render
     * 
     */

    render() {
        return (
            <div style={{ width: 300 }} >

                {this.state.flashMessage && <div className="alert alert-success" role="alert">
                    {this.state.flashMessage}
                </div>}
                {this.state.error && <div className="alert alert-danger" role="alert">
                    {this.state.error}
                </div>}
                {this.state.calling && <div className="alert alert-info" role="alert">
                    "calling en cours"
                </div>}

                <div className="form-group" >
                    <label className="mb-0" htmlFor="collection">Id de l'utilisateur</label>
                    <input type="text" className="form-control" id="uid" name="uid" placeholder="User id" onChange={this.handleChangeKeyItem} value={this.state.uid} />
                    <button className='btn btn-primary form-control' onClick={this.handleClickId}>Utiliser mon Id</button>
                    <label className="mb-0" htmlFor="collection">FCM Id à ajouter</label>
                    <input type="text" className="form-control" id="fcmId" name="fcmId" placeholder="FCM id" onChange={this.handleChangeKeyItem} value={this.state.fcmId} />
                </div>

                <div className="row">
                    <div className="col mb-3">
                    {!this.state.calling  &&
                        <button className="btn btn-success" onClick={this.callFunctionTest}>Ajouter</button>
                    }
                    {this.state.calling  &&
                        <div className="spinner-border text-primary" role="status">
                            <span className="sr-only">Loading...</span>
                        </div>
                    }
                    </div>
                </div>
            </div>
        );
    }
}