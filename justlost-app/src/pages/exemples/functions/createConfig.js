import React from 'react';

//import { reIndexAllDocumentInElastic } from '../../../services/elastic/calling'
/**
 * Exemple ave ElasticSearch afin d'indexer des documents et d'y faire des recheches
  * 
 */
export default class createConfig extends React.Component {

    constructor(props) {
        //Obligatoire quand on surcharge le constructeur
        super(props)

        //Je récupère la db et la stocke au niveau de mon object pour une faciliter d'écriture
        this.database = props.firebase.database();
        this.functions = props.firebase.functions;

        this.state = {
            calling: false,
            flashMessage: undefined,
            configInput : JSON.stringify({
                "keys" : {id: 1, code: "keys", triggerScore:10},
                "toy" : {id: 2, code: "toy", triggerScore:10},
                "wallet" : {id: 3, code: "wallet", triggerScore:15},
                "clothes" : {id: 4, code: "toclothes", triggerScore:10},
                "electronic" : {id: 5, code: "electronic", triggerScore:20},
                "jewelry" : {id: 6, code: "jewelry", triggerScore:30}
             })
        }

    }

    componentDidMount() {
        this.setState({});
    }
    /**
     */


    callFunctionTest = async (e) => {

        e.preventDefault()
        this.setState({ calling: true, flashMessage: undefined })

        console.log('this.state.configInput', typeof this.state.configInput, this.state.configInput)
        
        console.log('auth', this.props.firebase.auth)
        console.log('uid', this.props.firebase.auth.currentUser.uid)
    
        var createConfig = this.functions.httpsCallable('createConfig')
        await createConfig(JSON.parse(this.state.configInput))
        .then(result=>  this.setState({ calling: false, flashMessage: "Hello Ok"}))

        return
    }

    

    //****************************************************************************************** */


    /**
     * Gestion des handles des éléments du DOM (input, boutton)
     */
    handleChangeKeyItem = (e) => this.setState({ flashMessage: undefined, [[e.target.name]]: e.target.value })
    

    /** Le render
     * 
     */

    render() {
        return (
            <div style={{ width: 600 }} >

                {this.state.flashMessage && <div className="alert alert-success" role="alert">
                    {this.state.flashMessage}
                </div>}
                {this.state.error && <div className="alert alert-danger" role="alert">
                    {this.state.error}
                </div>}
                {this.state.calling && <div className="alert alert-info" role="alert">
                    "calling en cours"
                </div>}

                <div className="form-group" >
                    <label className="mb-0" htmlFor="collection">Config Input</label>
                    <textarea  rows="6"  className="form-control" id="configInput" name="configInput" placeholder="Elastic Index" onChange={this.handleChangeKeyItem} value={this.state.configInput} />
                </div>

                <div className="row">
                    <div className="col mb-3">
                    {!this.state.calling  &&
                        <button className="btn btn-success" onClick={this.callFunctionTest}>Charger</button>
                    }
                    {this.state.calling  &&
                        <div className="spinner-border text-primary" role="status">
                            <span className="sr-only">Loading...</span>
                        </div>
                    }
                    </div>
                </div>
            </div>
        );
    }
}