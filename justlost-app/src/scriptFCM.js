import React from 'react'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';



export async function requestUserNotificationPermission(firebase) {

    // Initialize the Firebase app by passing in the messagingSenderId
    var config = {
        messagingSenderId: "810377215216"
    };

    const messaging = firebase.messaging();
    return messaging.requestPermission()
        .then(async function () {
            console.log('Notification permission granted.');
            // TODO(developer): Retrieve an Instance ID token for use with FCM.
            return await messaging.getToken()
                .then(async function (currentToken) {
                    if (currentToken) {
                        console.log('Token: ' + currentToken)
                        /*
                        messaging.deleteToken(currentToken).
                        then(()=>console.log('token deleted'))
                        .catch(error=>console.log('token not deleted', error))
                        
                        */
                        await sendTokenToServer(firebase, currentToken);
                        return Promise.resolve(currentToken)
                    } else {
                        console.log('No Instance ID token available. Request permission to generate one.');
                        setTokenSentToServer(false);
                    }
                })
                .catch(function (err) {
                    console.log('An error occurred while retrieving token. ', err);
                    setTokenSentToServer(false);
                });
        })
        .catch(function (err) {
            console.log('Unable to get permission to notify.', err);
        });

}



export async function initMessaging(firebase) {
    // Initialize the Firebase app by passing in the messagingSenderId
    var config = {
        messagingSenderId: "810377215216"
    };

    const messaging = firebase.messaging();

    console.log('token', await messaging.getToken())


    // Handle incoming messages
    messaging.onMessage(function (payload) {
        toast.configure();
        if (!payload || !payload.data)
            return 

        const data = payload.data
        console.log('data', data)

        let divImage
        if (data && data.image)
            divImage = <img src={data.image} className="img-rounded" alt="" width="100" height="100"></img>
        else
            divImage = <div></div>

        const toastDetail = <div className="" >
            <img className="" src={data.image} style={{width: "80%"}} alt="Card image cap" />
            <div className="">
                <h5 className="">{data.title}</h5>
                <p className="">{data.description}</p>
                <a href="#" className="btn btn-primary">Vérifier</a>
            </div>
        </div>
        toast.info(toastDetail, {
            position: "top-center",
            autoClose: 10000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        });

        //   payload.notification.title

        console.log("Notification received: ", payload);

    });

    // Callback fired if Instance ID token is updated.
    messaging.onTokenRefresh(function () {
        messaging.getToken()
            .then(function (refreshedToken) {
                console.log('Token refreshed.');
                // Indicate that the new Instance ID token has not yet been sent 
                // to the app server.
                setTokenSentToServer(false);
                // Send Instance ID token to app server.
                sendTokenToServer(firebase, refreshedToken);
            })
            .catch(function (err) {
                console.log('Unable to retrieve refreshed token ', err);
            });
    });
}

// Send the Instance ID token your application server, so that it can:
// - send messages back to this app
// - subscribe/unsubscribe the token from topics
async function sendTokenToServer(firebase, currentToken) {
    if (!isTokenSentToServer()) {
        console.log('Sending token to server...');
        // TODO(developer): Send the current token to your server.

        const addFCMForUser = firebase.functions.httpsCallable('addFCMForUser')
        await addFCMForUser({ fcmId: currentToken, uid: firebase.auth.currentUser.uid })
        await setTokenSentToServer(true);
    } else {
        console.log('Token already sent to server so won\'t send it again ' +
            'unless it changes');
    }
}

function isTokenSentToServer() {
    //On force l'envoi
    return false
    return window.localStorage.getItem('sentToServer') == 1;
}

function setTokenSentToServer(sent) {
    window.localStorage.setItem('sentToServer', sent ? 1 : 0);
}