import React, { Component } from "react"
import createHttpError from "http-errors"

class Filters extends Component {
    constructor(props) {
        super(props)
        this.state = {
            filters: {},
            seletedFilter: props.defaultSelection || "_all",
            filtersConfig: props.filtersConfig || [{ label: "Tous", filter: "_all" }],
            fieldFilter: props.fieldFilter
        }
    }

    getLabel(filter) {
        return this.state.filtersConfig.find(config => (config.filter === filter)).label
    }

    UNSAFE_componentWillReceiveProps(newProps) {
        const newFilters = {}
        const fieldFilter = this.state.fieldFilter
        const shortDeclarationList = newProps.shortDeclarationList
        shortDeclarationList.forEach(shortDeclaration => {
            const curValue = newFilters[shortDeclaration[fieldFilter]]
            newFilters[shortDeclaration[fieldFilter]] = curValue ? curValue + 1 : 1
        });
        this.setState({ filters: newFilters })
    }

    handleFilterSelected = (filter) => {
        console.log('seletedfilter', filter)
        this.setState({ seletedFilter: filter }, this.props.handleFilterSelected(filter))
    }

    render3() {
        return <div>filters</div>
    }
    render() {

        return (

            <ul className="nav nav-tabs">
                <li className="nav-item" key="_all" >
                    <a href="#"  className={this.state.seletedFilter && this.state.seletedFilter === "_all" ? "nav-link active" : "nav-link"} onClick={() => this.handleFilterSelected("_all")}>
                        {this.getLabel("_all")} <span className="badge badge-light">{Object.entries(this.state.filters).reduce((cumul, [filter, count]) => { return cumul + count }, 0)}</span>
                    </a>
                </li>

                {Object.entries(this.state.filters).map(([filter, count]) =>
                    <li  key={filter} className="nav-item">
                    <a href="#" className={"nav-link " + this.state.seletedFilter && this.state.seletedFilter === filter ? "nav-link active" : "nav-link"} onClick={() => this.handleFilterSelected(filter)}>
                        {this.getLabel(filter)} <span className="badge badge-light">{count}</span>
                    </a>
                    </li>
                )}
            </ul>
        )
    }

}
export default Filters

