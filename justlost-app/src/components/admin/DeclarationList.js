import React, { Component, useState } from "react"
import { withFirebase } from '../Firebase';
import { Map, TileLayer, Circle } from "react-leaflet"

/**
 * Main issues
 * 
 * -no pagination, memory coast on large number of item load
 * -each map loaded, need to load a map only by request
 * -segment in components
 * 
 */

class DeclarationList extends Component {

    constructor(props) {
        super(props)
        this.database = props.firebase.database();

        this.state = {
            documents: [],
            currentSelectedDeclaration: undefined
        }
        this.toogleDetail = []
        this.generateDeclarationList = this.generateDeclarationList.bind(this)
        this.generateMatchingList = this.generateMatchingList.bind(this)
        this.currentSelectMatch =undefined
    }

    /**
     * React lifecycles
     */
    /*
        componentDidMount() {
            this.getAll()
                .on('value', snapshot => {
                    let documents = []
                    const documentsObject = snapshot.val();
    
                    if (documentsObject) {
                        documents = Object.keys(documentsObject).map(key => ({
                            ...documentsObject[key],
                            id: key,
                        }))
                    }
    
                    this.setState({
                        documents
                    });
                });
    
        }*/

    componentWillUnmount = () => this.removeEventListener()

    /**
     * Firebase stuff
     */

    getAll = () => this.database.ref("declarations");

    getOne = (id) => this.database.ref("declarations/" + id);

    removeEventListener = () => this.database.ref("declarations").off();

    /**
     * Compnent methods
     */
    generateDeclarationList() {
        let daCount = 0;
        let firstCardMarginTop = ""
        return this.props.documents.map(currentDeclaration => {

            // Set a padding top for all card except the first one
            if (daCount !== 0) {
                firstCardMarginTop = "mt-3"
            } else {
                firstCardMarginTop = "mt-0"
            }
            daCount++

            // Get coords information with handle of capitalization issue
            let coords = { x: 0, y: 0 }
            if (currentDeclaration.eventZone.userSelectionArea.longitude) {
                coords.x = parseFloat(currentDeclaration.eventZone.userSelectionArea.longitude, 10)
            } else {
                if (currentDeclaration.eventZone.userSelectionArea.Longitude) {
                    coords.x = parseFloat(currentDeclaration.eventZone.userSelectionArea.Longitude, 10)
                } else {
                    coords.x = 0
                }
            }
            if (currentDeclaration.eventZone.userSelectionArea.latitude) {
                coords.y = parseFloat(currentDeclaration.eventZone.userSelectionArea.latitude, 10)
            } else {
                if (currentDeclaration.eventZone.userSelectionArea.Latitude) {
                    coords.y = parseFloat(currentDeclaration.eventZone.userSelectionArea.Latitude, 10)
                } else {
                    coords.y = 0
                }
            }

            const borderDeclaration =  this.state.currentSelectedDeclaration &&
                this.state.currentSelectedDeclaration.declarationId == currentDeclaration.declarationId ? { backgroundColor: 'aliceblue'} : { backgroundColor: "white" }
            return (
                <div key={currentDeclaration.declarationId} className="card mb-2 p-2" data-parent="#accordion" style={borderDeclaration} data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample" >


                    <div className="row d-flex justify-content-between" onClick={() => {

                        this.setState({ currentSelectedDeclaration: currentDeclaration }, this.props.onSelect(currentDeclaration))
                        //On ferme le précédent s'il y en a un et qu'il est différent de l'actuel
                        this.toogleDetail[currentDeclaration.declarationId].click()
                    }

                    }>
                        <div className="col-10">
                            <span className="text-right text-danger align-middle">
                                <i className="fa fa-map-marker" style={{ fontSize: "30px", color: "77B7FE" }}></i>
                            </span>
                            <span className="align-bottom" style={{ fontSize: ".7rem" }}>
                                {currentDeclaration.declarationMapNumber && `${currentDeclaration.declarationMapNumber} `}
                            </span>
                            <span className="text-left font-weight-normal align-middle m-2">{currentDeclaration.userDescription}</span>
                        </div>
                        <div className="col text-right align-middle">
                            <span className="">{capitalize(currentDeclaration.category.label)}</span>
                        </div>
                    </div>


                    <div className="" id="collapseExample">

                        <div>

                            {/* Top of the card, basic informaiton */}

                            <div
                                className="accordion"
                                id="accordionDeclaration"
                            >

                                {/* End first section */}

                                {/* Second section */}
                                <div className="border-0">
                                    <div
                                        className="card-header  d-none"
                                        id={"declarationESMatching-" + daCount}
                                    >
                                        <h6 className="mb-0">
                                            <button
                                                className="btn btn-link"
                                                type="button"
                                                data-toggle="collapse"
                                                data-target={"#collapseESMatching-" + daCount}
                                                aria-expanded="false"
                                                aria-controls={"collapseESMatching-" + daCount}
                                                ref={input => { this.toogleDetail[currentDeclaration.declarationId] = input }}
                                            >
                                                Résultat des possibles correspondances
                                        </button>
                                        </h6>
                                    </div>

                                    <div
                                        id={"collapseESMatching-" + daCount}
                                        className="collapse mt-3"
                                        aria-labelledby={"declarationESMatching-" + daCount}
                                        data-parent="#accordionDeclaration"
                                    >



                                        <div className="row" >
                                            <div className="col">
                                                <h6 className="card-subtitle mb-3 text-muted">
                                                    {getTypeLabel(currentDeclaration.declarationType)} par {currentDeclaration.declarationOwner.FullName} ({currentDeclaration.declarationOwner.email}) le {new Intl.DateTimeFormat('fr-FR').format(currentDeclaration.eventDate.stringDate)} - {currentDeclaration.declarationNumber || currentDeclaration.declarationId}
                                                </h6>
                                            </div>

                                        </div>





                                        {/* Corps of the card, useful information */}
                                        <p
                                            className="card-text"
                                        >

                                        </p>
                                        <div className="">
                                            <ul className="list-group">
                                                {this.generateMatchingList(currentDeclaration, { ...currentDeclaration.adminMatchingList, ...currentDeclaration.esMatchingList })}
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                {/* End second section */}
                            </div>
                            {/* End corps section */}

                        </div>

                    </div>

                </div>
            )
            // End return
        })
        // End map
    }

    getLastNotification(match){
        if (!match.notificationList)
            return undefined
        const notificationList = Object.values(match.notificationList)
        if (!notificationList)
            return undefined

        //Trouve la notification la plus récente
        const mostRecentNotification =  notificationList.reduce((max, p) => p.sendTimeStamp > max.sendTimeStamp ? p : max, notificationList[0]);
        return mostRecentNotification
    }
    


    generateMatchingList(currentDeclaration, elasticSearchMatchingList) {

        // Convert json to array
        let arrOfMatching = []
        for (let ii in elasticSearchMatchingList) {
            arrOfMatching.push(elasticSearchMatchingList[ii]);
        }
        //On va les trier par scoring croissant
        arrOfMatching.sort((a, b) => b.score - a.score)

        if (arrOfMatching.length === 0) {
            return (
                <li className="list-group-item d-flex justify-content-center align-items-center">
                    <h2><span className="badge badge-secondary badge-pill">Aucune correspondance trouvée.</span></h2></li>
            )
        }

        return arrOfMatching.map((currentMatch, index) => {
            const esNumber = "T" + (index + 1)
            const decisionFormat = (decision) => {
                return {
                    declarationId: currentDeclaration.declarationId,
                    matchId: currentMatch.foundDeclarationId,
                    decision,
                    decisionOrigin: 'admin'
                }
            }
            return (
                
                <li
                    className="list-group-item ist-group-item-action d-flex justify-content-between align-items-center"
                    key={currentMatch.foundDeclarationId}
                    onClick={() => { this.currentSelectMatch = currentMatch.esId; this.props.onMatchedSelected(currentMatch.foundDeclarationId, 'selected', esNumber)}}
                    style={{backgroundColor: this.currentSelectMatch === currentMatch.esId ? '#E5F7F4': '#FFFFFF'}}
                >
                    <div  className="d-flex justify-content-between align-items-center">
                        <button type="button" className="btn  mr-3" onClick={() => this.props.onMatchedSelected(currentMatch.foundDeclarationId , 'viewPosition', esNumber)}>
                            <span className="text-right text-danger align-middle">
                                <i className="fa fa-map-marker" style={{ fontSize: "30px", color: "#77B7FE" }}></i>
                            </span>
                            <span className="align-bottom" style={{ fontSize: ".7rem" }}>
                                {esNumber}
                            </span>

                        </button>

                        <h2><span className="badge badge-warning badge-pill">{parseFloat(currentMatch.score).toFixed(2)}</span></h2>
                    </div>


                    {(currentMatch.decision !== 'matched' && currentMatch.decision !== 'toNotificateAndMatch') && currentMatch.decision !== 'rejected' &&
                        <div>
                            {this.getLastNotification(currentMatch) && <span className="mr-2" style={{fontSize:'.8rem'}}></span>}
                            <button type="button" className="btn btn-success mr-3" 
                                    onClick={() => 
                                        {
                                            this.setState({pending_notification:true});
                                            this.props.handleDecision(decisionFormat('toNotificate')).then(()=>this.setState({pending_notification:false}))}
                                        }>
                                {this.state.pending_notification  &&
                                <div className="spinner-border" role="status">
                                    <span className="sr-only">Loading...</span>
                                </div>
                                }
                                {! this.state.pending_notification  &&
                                    <i class="fa fa-bell" aria-hidden="true"></i>
                                }
                            </button>
                            <div className="btn-group" role="group" aria-label="Basic example">
                                <button type="button" className="btn btn-primary" 
                                    onClick={() => 
                                        {
                                            this.setState({pending_matched:true});
                                            this.props.handleDecision(decisionFormat('matched')).then(()=>this.setState({pending_matched:false}))}
                                        }>
                                                                        {this.state.pending_matched  &&
                                <div className="spinner-border" role="status">
                                    <span className="sr-only"></span>
                                </div>
                                }
                                {! this.state.pending_matched  && "Trouvé !"}
                              </button>

                                <button type="button" className="btn btn-danger" 
                                    onClick={() => 
                                        {
                                            this.setState({pending_rejected:true});
                                            this.props.handleDecision(decisionFormat('rejected')).then(()=>this.setState({pending_rejected:false}))}
                                        }>
                                                                        {this.state.pending_rejected  &&
                                <div className="spinner-border" role="status">
                                    <span className="sr-only"></span>
                                </div>
                                }
                                {! this.state.pending_rejected  && "Ignorer"}
                              </button>

                            </div></div>}
                    {(currentMatch.decision === 'matched' || currentMatch.decision ==='toNotificateAndMatch') && <span className="text-success">L'objet a été retrouvé !</span>}
                    {currentMatch.decision === 'rejected' && <span className="text-secondary" >Ne correspond pas</span>}

                </li>


            )
        })
    }

    render() {
        return (
            <div>

                {/* To avoid style scope issues, some style are defined for the component itself, works as a temporary fix, need a clean solution later */}

                {/* Bootstrap need its style and script */}
                <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossOrigin="anonymous"></link>
                <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossOrigin="anonymous"></script>
                <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossOrigin="anonymous"></script>
                <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossOrigin="anonymous"></script>
                {/* End of bootstrap needed things */}

                {/* Leaflet need its style and script */}
                <link
                    rel="stylesheet"
                    href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
                    integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
                    crossOrigin=""
                />
                <script
                    src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
                    integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
                    crossOrigin=""
                ></script>
                {/* End of leaftlet needed things */}


                <div className="row">
                    <div className="col">

                        {/* Corps */}
                        {
                            this.props.documents.length !== 0
                                ?
                                <div
                                    className=""
                                    style={{
                                        width: "100%"
                                    }}
                                >
                                    <ul className="list-group" id="accordion">
                                        {this.generateDeclarationList()}
                                    </ul>
                                </div>
                                :
                                <div
                                    className="text-center"
                                    style={{
                                        width: "100%"
                                    }}
                                >
                                    <div className="d-flex justify-content-center align-items-center">
                                        <h2><span className="badge badge-secondary badge-pill">Sélectionnez une catégorie pour voir la liste des objets perdus non retrouvés.</span></h2></div>

                                </div>
                        }
                        {/* End corps */}

                    </div>
                </div>
            </div>

        );
    }
}

function getTypeLabel(type) { return type === 'lost' ? 'perdu' : 'trouvé' }
function capitalize(str1) {
    return str1.charAt(0).toUpperCase() + str1.slice(1);
}


export default withFirebase(DeclarationList)