import React, { Component, useState } from "react"
import { withFirebase } from '../Firebase';
import { Map, TileLayer, Circle } from "react-leaflet"

/**
 * Main issues
 * 
 * -no pagination, memory coast on large number of item load
 * -each map loaded, need to load a map only by request
 * -segment in components
 * 
 */

class DeclarationList extends Component {

    constructor(props) {
        super(props)
        this.database = props.firebase.database();

        this.state = {
            documents: []
        }

        this.generateDeclarationList = this.generateDeclarationList.bind(this)
        this.generateMatchingList = this.generateMatchingList.bind(this)
    }

    /**
     * React lifecycles
     */
/*
    componentDidMount() {
        this.getAll()
            .on('value', snapshot => {
                let documents = []
                const documentsObject = snapshot.val();

                if (documentsObject) {
                    documents = Object.keys(documentsObject).map(key => ({
                        ...documentsObject[key],
                        id: key,
                    }))
                }

                this.setState({
                    documents
                });
            });

    }*/

    componentWillUnmount = () => this.removeEventListener()

    /**
     * Firebase stuff
     */

    getAll = () => this.database.ref("declarations");
    
    getOne = (id) => this.database.ref("declarations/" + id);

    removeEventListener = () => this.database.ref("declarations").off();

    /**
     * Compnent methods
     */
    generateDeclarationList() {
        let daCount = 0;
        let firstCardMarginTop = ""
        return this.props.documents.map(currentDeclaration => {

            // Set a padding top for all card except the first one
            if (daCount !== 0) {
                firstCardMarginTop = "mt-3"
            } else {
                firstCardMarginTop = "mt-0"
            }
            daCount++

            // Get coords information with handle of capitalization issue
            let coords = { x:0, y:0 }
            if (currentDeclaration.eventZone.userSelectionArea.longitude) {
                coords.x = parseFloat(currentDeclaration.eventZone.userSelectionArea.longitude, 10)
            } else {
                if (currentDeclaration.eventZone.userSelectionArea.Longitude) {
                    coords.x = parseFloat(currentDeclaration.eventZone.userSelectionArea.Longitude, 10)
                } else {
                    coords.x = 0
                }
            }
            if (currentDeclaration.eventZone.userSelectionArea.latitude) {
                coords.y = parseFloat(currentDeclaration.eventZone.userSelectionArea.latitude, 10)
            } else {
                if (currentDeclaration.eventZone.userSelectionArea.Latitude) {
                    coords.y = parseFloat(currentDeclaration.eventZone.userSelectionArea.Latitude, 10)
                } else {
                    coords.y = 0
                }
            }

            return (
                <div
                    className={"card " + firstCardMarginTop}
                    key={currentDeclaration.declarationId}
                >
                    <div className="card-header">
                        <img
                        className="d-inline-block"
                            src={currentDeclaration.category.image}
                            style={{
                                width:30,
                                height:30
                            }}
                        />
                        {" - "}
                        {currentDeclaration.category.label}
                        <div className="float-right">
                            État de la déclaration :&nbsp; {/* "&nbsp;" force to put a blank space*/}
                            <span className="badge badge-danger">{currentDeclaration.declarationType}</span>
                            {/*<span className="badge badge-success">{currentDeclaration.declarationType}</span>*/}
                        </div>
                    </div>
                    <div className="card-body">
                    
                        {/* Top of the card, basic informaiton */}
                        <h3
                            className="card-title"
                        >
                            Nom de la déclaration/objet en question
                        </h3>

                        <h6
                            className="card-subtitle mb-2 text-muted"
                        >
                            perdu par {currentDeclaration.declarationOwner.FullName} ({currentDeclaration.declarationOwner.email}) le {currentDeclaration.declarationDate.numericDate}
                        </h6>
                        {/* End top section */}

                        {/* Corps of the card, useful information */}
                        <p
                            className="card-text"
                        >
                            Description : "{currentDeclaration.userDescription}"
                        </p>

                        <div
                            className="accordion"
                            id="accordionDeclaration"
                        >
                            {/* First section */}
                            <div className="card">
                                <div
                                    className="card-header"
                                    id={"declarationLostArea-" + daCount}
                                >
                                    <h6 className="mb-0">
                                        <button
                                            className="btn btn-link"
                                            type="button"
                                            data-toggle="collapse"
                                            data-target={"#collapseMap-" + daCount}
                                            aria-expanded="false"
                                            aria-controls={"collapseMap-" + daCount}
                                        >
                                            Zone de la déclaration
                                        </button>
                                    </h6>
                                </div>

                                <div
                                    id={"collapseMap-" + daCount}
                                    className="collapse"
                                    aria-labelledby={"declarationLostArea-" + daCount}
                                    data-parent="#accordionDeclaration"
                                >
                                    <div className="card-body">
                                        <p
                                            className="card-text"
                                        >
                                            Indication de l'utilisateur : "{currentDeclaration.eventZone.userInput}"
                                        </p>
                                        <Map
                                            center={[
                                                coords.x,
                                                coords.y
                                            ]}
                                            zoom={13}
                                            scrollWheelZoom={false}
                                            style={{
                                                width: "100%",
                                                height: "400px"
                                            }}
                                        >
                                        <Circle
                                            center={[
                                                coords.x,
                                                coords.y
                                            ]}
                                            radius={1000}
                                        >
                                            <TileLayer
                                                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                                                attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                                            />
                                        </Circle>
                                    </Map>
                                    </div>
                                </div>
                            </div>
                            {/* End first section */}

                            {/* Second section */}
                            <div className="card">
                                <div
                                    className="card-header"
                                    id={"declarationESMatching-" + daCount}
                                >
                                    <h6 className="mb-0">
                                        <button
                                            className="btn btn-link"
                                            type="button"
                                            data-toggle="collapse"
                                            data-target={"#collapseESMatching-" + daCount}
                                            aria-expanded="false"
                                            aria-controls={"collapseESMatching-" + daCount}
                                        >
                                            Résultat des possibles correspondances
                                        </button>
                                    </h6>
                                </div>

                                <div
                                    id={"collapseESMatching-" + daCount}
                                    className="collapse"
                                    aria-labelledby={"declarationESMatching-" + daCount}
                                    data-parent="#accordionDeclaration"
                                >
                                    <div className="card-body">
                                        <ul className="list-group">
                                            {this.generateMatchingList(currentDeclaration.esMatchingList)}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            {/* End second section */}
                        </div>
                        {/* End corps section */}

                    </div>
                    <div className="card-footer">
                        <p className="card-text text-muted">déclaration n°{currentDeclaration.declarationId}</p>
                    </div>
                </div>
            )
            // End return
        })
        // End map
    }

    generateMatchingList(elasticSearchMatchingList) {
        
        // Convert json to array
        let arrOfMatching = []
        for (let ii in elasticSearchMatchingList) {
            arrOfMatching.push(elasticSearchMatchingList[ii]);
        }

        if (arrOfMatching.length === 0) {
            return (
                <p>Pas de résultat</p>
            )
        }
        
        return arrOfMatching.map(currentMatch => {
            return (
                <li
                    className="list-group-item"
                    key={currentMatch.foundDeclarationId}
                >

                    {/* firebase call here, to get declaration by its id */}

                    {/* Content */}
                    <div className="float-left">
                        foundDeclarationId : {currentMatch.foundDeclarationId}
                    </div>

                    {/* Action */}
                    <div className="float-right">
                        <button className="btn btn-primary">action 1</button>
                        <button className="btn btn-secondary">action 2</button>
                    </div>
                </li>
            )
        })
    }

    render() {
        return (
            <div>

                {/* To avoid style scope issues, some style are defined for the component itself, works as a temporary fix, need a clean solution later */}

                {/* Bootstrap need its style and script */}
                <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"></link>
                <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
                <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
                <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
                {/* End of bootstrap needed things */}
                
                {/* Leaflet need its style and script */}
                <link
                    rel="stylesheet"
                    href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
                    integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
                    crossorigin=""
                />
                <script
                    src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
                    integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
                    crossorigin=""
                ></script>
                {/* End of leaftlet needed things */}

                <div className="container">
                    <div className="row">
                        <div className="col">

                            {/* Corps */}
                            {
                                this.props.documents.length !== 0
                                ?
                                <div
                                    className="my-5"
                                    style={{
                                        width: "100%"
                                    }}
                                >
                                    {this.generateDeclarationList()}
                                </div>
                                :
                                <div
                                    className="text-center my-5"
                                    style={{
                                        width: "100%"
                                    }}
                                >
                                    <p>Récuperation des déclarations</p>
                                    <div class="spinner-border" role="status">
                                        <span class="sr-only">Loading...</span>
                                    </div>
                                </div>
                            }
                            {/* End corps */}

                        </div>
                    </div>
                </div>
            </div>
          );
    }
}

export default withFirebase(DeclarationList)