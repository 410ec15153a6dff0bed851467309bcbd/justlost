'use strict'
import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';
import MarkerClusterer from '@google/markerclusterer'
import { withFirebase } from '../../components/Firebase';
const markerLost = require('../../images/markerLost.png')
const markerFound = require('../../images/markerFound.png')


//Si l'on souhaite, on peut mettre un composant dans le children de GoogleMapReact et il sera affiché
//il faut qu'il ai des cordonnées GPS
//const AnyReactComponent = ({ text }) => <div>{text}</div>;

class GoogleMaps extends Component {


  constructor(props) {
    super(props)
    this.state = {
      
      positionToMarker: false,
      loading: true
    }
    // Liste des markeurs créés à partir de la locations
    // Marche de pair avec this.locations
    this.locations= []
    this.markers = []
    this.markerClusterers = undefined
    this.declarationCircles = new Map()

  }

  createDeclarationsLocation() {
    let newLocations = []

    //console.log('this.props.declarationFoundMapList', this.props.declarationFoundMapList)

    const declarationList = [...this.props.declarationList, ...this.props.declarationFoundMapList]
    //console.log('declarationList à créer dans map', declarationList)
    
    declarationList.forEach(declaration => {
      //Todo , test à enlever quand on aura systématiquement un eventZone.userSelectionArea sur toutes les déclarations.
      if (!declaration.eventZone.userSelectionArea)
        return
      const userSelectionArea = declaration.eventZone.userSelectionArea

      //On fabrique l'objet location
      //Todo : ternaire à enlever quand latitude et longitude seront en minuscule
      const location = {
        declarationId: declaration.declarationId,
        declarationMapNumber: declaration.declarationMapNumber,
        declaration: declaration, //On met toute la déclaration
        declarationType: declaration.declarationType,
        lat: userSelectionArea.latitude,
        lng: userSelectionArea.longitude,
        label: declaration.label ? declaration.label : declaration.userDescription,
      }

      newLocations[location.declarationId] = location
      //On met à jour le state
      
    })
    //console.log(newLocations, newLocations)
  return newLocations
  }

  


//Initialisation des objects natifs google !!!
// On s'en sert pour les marqueurs en autre. 
setGoogleMapRef(map, maps) {
  this.googleMapRef = map
  this.googleRef = maps
}

//Ajouter les marqueurs à la carte
setLocationsToMap(locations) {

  const map = this.googleMapRef
  const callbackOnClick = (marker) => { console.log('clické', marker.declarationMapNumber); this.centerToMarker(marker.declarationMapNumber) }
  let markers = locations && Object.values(locations).map((location) => {
    return new this.googleRef.Marker({
      position: location,
      label: { text: '' + location.declarationMapNumber, fontSize: '.7rem', fontFamily: 'Roboto', labelStyle: { opacity: 0.75 } },
      location: location,
      icon: location.declarationType === 'lost' ? markerLost : markerFound,
      clickable: true,
      onClick: callbackOnClick
    })
  })
  this.markers = markers

  const markerClusterer = new MarkerClusterer(map, markers, {
    imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m',
    gridSize: 10,
    minimumClusterSize: 2
  })
  this.markerClusterers = markerClusterer
}

UNSAFE_componentWillReceiveProps(newProps){
  if (newProps && newProps.positionToMarker) {
    this.centerToMarker(newProps.positionToMarker)
  }
}

goToPosition(position)
{
  const map = this.googleMapRef
  map.setZoom(12)
  map.panTo(position);
}

centerToMarker(number){
  const locationCenter = this.markers.filter(marker => marker.location.declarationMapNumber == number)
  if (locationCenter.length==0)
    return 
  this.goToPosition(locationCenter[0].position)
  this.setState({ positionToMarker: true })

}

clearAllMarkers(){
  this.markerClusterers && this.markerClusterers.clearMarkers()
  this.markers.forEach(marker => marker.setMap(null))
  this.markerClusterers = undefined
  this.markers=[]
}

render2() {return <div>map</div>}

render() {
  const map = this.googleMapRef

  this.clearAllMarkers()

  
  if (this.googleMapRef){
    const locations = this.createDeclarationsLocation()
    this.setLocationsToMap(locations)
  }

  if (this.props.goToPosition)
    this.goToPosition(this.props.goToPosition)


  if (this.googleRef && this.props.declarationCircles && this.props.declarationCircles.length > 0) {
    this.declarationCircles.forEach((cityCircle, key) => 
    {
      cityCircle.setMap(null); 
      this.declarationCircles.delete(key)
    });

    this.props.declarationCircles.forEach(declaration => {
      var cityCircle
      if (!this.declarationCircles.has(declaration.declarationId)) {
        cityCircle = new this.googleRef.Circle({
          strokeColor: '#F00000',
          strokeOpacity: 0.5,
          strokeWeight: 1,
          fillColor: declaration.declarationType === 'lost' ? '#FF0000' : '#77B7FE',
          fillOpacity: declaration.declarationType === 'lost' ? 0.25 : 0.15,
          map: map,
          center: {
            lat: declaration.eventZone.userSelectionArea.latitude,
            lng: declaration.eventZone.userSelectionArea.longitude
          },
          radius: declaration.eventZone.searchRadius
        })
        //console.log('Circle created', cityCircle)
        this.declarationCircles.set(declaration.declarationId, cityCircle)
      }
    });

  }


  //return <div>map</div>
  return (
    // Important! Always set the container height explicitly
    <GoogleMapReact
      bootstrapURLKeys________________AENLEVER={{ key: `${process.env.REACT_APP_GOOGLE_MAPS_API}` }}
      defaultCenter={{
        lat: 50.6356739,
        lng: 3.0629969
      }}
      defaultZoom={this.props.zoom || 10}
      onGoogleApiLoaded={({ map, maps }) => this.setGoogleMapRef(map, maps)}
      yesIWantToUseGoogleMapApiInternals={true}
    >
    </GoogleMapReact>
  );
}
    }

export default withFirebase(GoogleMaps);



