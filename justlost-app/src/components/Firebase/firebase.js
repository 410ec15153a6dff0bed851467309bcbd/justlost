import app from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';
 
//Lorsque l'appli est lancé en express, ce code permet d'accéder au .env
const dotenv = require('dotenv')
dotenv.config();

const devConfig = {
  apiKey: process.env.REACT_APP_API_KEY,
  authDomain: process.env.REACT_APP_AUTH_DOMAIN,
  databaseURL: process.env.REACT_APP_DATABASE_URL,
  projectId: process.env.REACT_APP_PROJECT_ID,
  storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_MESSAGING_SENDER_ID,
  region : process.env.REACT_APP_FUNCTIONS_REGION,
  appId :  process.env.REACT_APP_APP_ID
};

const prodConfig = devConfig
const config =
  process.env.NODE_ENV === 'production' ? prodConfig : devConfig;
 
class Firebase {
  constructor() {
    app.initializeApp(config)
    console.log('Contructeur de firebase', app)
    this.auth = app.auth();
    this.database = () => app.database();
    this.storage = app.storage && app.storage();
    this.functions = app.functions();
    this.messaging = () => app.messaging();

  }
}

export default Firebase;



