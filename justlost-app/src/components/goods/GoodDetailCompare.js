import React from 'react'
import { withFirebase } from "../Firebase";


class GoodDetailCompare extends React.Component {



    render() {
        const formatTags = (tags) => tags.filter(([tag, action]) => action === 'used').map(([tag, action]) => tag)
        const { declaration, declarationCompared } = this.props
        const tags = formatTags(declaration.tags)
        const tagsCompared = formatTags(declarationCompared && declarationCompared.tags ? declarationCompared.tags : [])
        return (
            <div className="p-2" style={{ margin: "4px 14px 0 15px" }}>
                <label htmlFor="tags">Liste des tags</label>
                <div name="tags" id="tags" className="mb-2">
                    {tags.map((tag, index) => {
                        const textColor = tagsCompared && tagsCompared.includes(tag) ? "badge-danger" : "badge-light"
                        return <span key={index} className={"mr-1 badge " + textColor}>{tag}</span>
                    })
                    }
                </div>

                <div >
                    <div className="row">
                        <div className="col" style={{width:'100%', height:'100%'}}>
                            <div className="img-responsive"  style={{height:'auto', maxWidth:'500px'}} >
                                <img
                                    className={"img-fluid rounded"}
                                    style={{ width: '100%' }}
                                    src={declaration.thumbnailURL}
                                    alt="" />
                            </div>
                        </div>
                    </div>
                </div>
                </div>
        )

    }

}



export default withFirebase(GoodDetailCompare)