import React from "react";
import ReactWizard from "react-bootstrap-wizard";
import { Container, Row, Col } from "reactstrap";
import UploadImage from '../Storage/UploadImage'
import MapApiMisc from '../Map/mapApiMisc'
import Tags from './tags/Tags'
import "bootstrap/dist/css/bootstrap.css";
import './GoodDetailWizard.css';

import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import { withFirebase } from "../Firebase";
import { requestUserNotificationPermission } from '../../scriptFCM'
import InputRange from 'react-input-range';
import * as ROUTES from '../../constants/routes';
import { NavLink } from 'react-router-dom';

const categories = [
  { id: 2, code: "toy", label: "Jouet", lostLabel: "Perte d'un jouet", foundLabel: "J'ai trouvé un jouet", description: "Doudou, peluche, ...", image: require("../../images/new/toy.jpg") },
  { id: 5, code: "electronic", label: "Electronique", lostLabel: "Perte d'un appareil électronique", foundLabel: "J'ai trouvé votre appareil", description: "Téléphone, ordinateur, appareil photo, ...", image: require("../../images/new/phone.jpg") },
  { id: 4, code: "clothes", label: "Vêtement", lostLabel: "Perte d'un vêtement", foundLabel: "J'ai trouvé un vêtement", description: "Veste, gants, blouson, ...", image: require("../../images/new/clothes.jpg") },
  { id: 3, code: "wallet", label: "Petit objet", lostLabel: "Perte d'un sac/portefeuille", foundLabel: "J'ai trouvé un sac/portefeuille", description: "Sacs, portefeuille, lunettes, documents, ...", image: require("../../images/new/wallet.png") },
  { id: 6, code: "jewelry", label: "Bijou", lostLabel: "Perte d'un bijou", foundLabel: "J'ai trouvé un bijou", description: "Chaîne, bague, boucles d'oreilles ...", image: require("../../images/new/jewels.jpg") },
  { id: 1, code: "keys", label: "Clés", lostLabel: "Perte de clé", foundLabel: "J'ai trouvé des clés", description: "Clés, trousseau, ...", image: require("../../images/new/key.jpg") },

]

const styleHeight = { minHeight: '460px' }

class CategorySelection extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      firstStep: "first step here",
      category: undefined
    };
    this.handleCategorieSelection = this.handleCategorieSelection.bind(this)
  }

  handleCategorieSelection(category) {
    this.setState({ category }, () => this.props.goNext()) //Une fois la catégorie sélectionnée, on passe de suite à l'étape suivante

  }

  render() {
    return <div style={styleHeight}>
      <div className="form-group">
        <label htmlFor="categorylist">
          Cliquez sur l'image correspondant à la catégorie d'objet {getTypeLabel(this.props.declarationType)}
        </label>
        {this.state.error &&
          <div class="alert alert-danger" role="alert">
            {this.state.error}
          </div>
        }
        <div style={{ margin: "10px 0px 0px 0px", padding: "10px 0px 0px 0px"}}>
          <div className="d-flex justify-content-between align-content-center flex-wrap">
            {categories.map(cat => {
              const border = this.state && this.state.category && this.state.category.id && this.state.category.id === cat.id ? "border border-primary" : ""
              return (
                <div key={cat.id} className="mb-2 p-1 shadow bg-white rounded" onClick={() => this.handleCategorieSelection(cat)}>

                    <img 
                      className={"img-fluid rounded image-cat " + border}
                      style={{ width: '65%' }}
                      src={cat.image}
                      alt={cat.description} />
                    <div className="description mt-1 mr-1 ml-1" style={{fontSize:".9rem"}}  >
                      {cat.description}
                    </div>
                </div>)
            }
            )
            }
          </div>
        </div>
      </div>
    </div>
  }

  isValidated() {
    if (!this.state.category) {
      this.setState({ error: "Veuillez selectionner une catégorie d'objet " + getTypeLabel(this.props.declarationType) })
      setTimeout(() => this.setState({ error: undefined }), 4000)
      return false
    }
    this.props.calcNext(this.props.declarationType === 'lost' ? this.state.category.lostLabel : this.state.category.foundLabel)
    return true;
  }
}
class MessageStep extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      message: "",
    };
    this.handleChange = this.handleChange.bind(this)
  }
  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value })
  }
  render() {
    return <div style={styleHeight} >
      <form onSubmit={this.handleSubmit}>
        <div className="form-group">
          <label htmlFor="message">
            Message personnel :
					</label>
          <textarea placeholder="Message de l'objet" style={{ whiteSpace: 'pre-line' }} className="form-control" id="message" name="message" rows="3" onChange={this.handleChange}></textarea>
        </div>
      </form></div>
  }
}

class DescriptionStep extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      description: "",
      tags: new Map(props.tags || [])
    };
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }


  generateTagsFromString(text) {
    Array.prototype.unique = function () {
      return this.filter(function (value, index, self) {
        return self.indexOf(value) === index;
      });
    }

    const excludedWords = ["ce", "eu", "ou", "se", "qui", "que", "d'un", "d'une", "elle", "il", "est", "sont", "on", "j'ai", "ma", "et", "avec", "pour", "du", "de", "des", "un", "une", "la", "le", "les", "en", "à", "son", "sa", "ne", "pas", "vous", ".", "merci", ',']
    const rawWords = text.split(' ');

    let tags = rawWords.filter(word => excludedWords.indexOf(word.toLowerCase()) === -1 && word.length > 1)
    return tags.unique()
  }


  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value })
  }
  handleSubmit(e) {
    e.preventDefault()
    let description = this.state.description

    const existingTags = this.state.tags
    const newTags = new Map(existingTags)
    //Attention uniquement pour la description
    const tagsRaw = this.generateTagsFromString(description.replace("\n", ""))//Supression des retours à la ligne


    //On va mettre à jour la liste avec les nouveaux tags

    tagsRaw.forEach(tagName => {

      if (!existingTags.has(tagName))
      //Nouveau tag
      {
        newTags.set(tagName, 'used')
        //console.log('Est nouveau =>>>>>>>>>>>>>>', tagName)
      }
    });

    this.setState({ tags: newTags })
  }

  isValidated() {
    if (this.state.tags.size == 0) {
      this.setState({ error: "Veuillez ajouter des tags de description pour retrouver votre objet." })
      setTimeout(() => this.setState({ error: undefined }), 4000)
      return false
    }
    const label = "" //this.state.tags.slice(1, 2).join('-')
    this.props.calcNext(label)
    return true;
  }

  manageTagUpdating = ([tag, action]) => {
    const existingTags = this.state.tags
    const newTags = new Map(existingTags)
    if (existingTags.has(tag))
      newTags.set(tag, action)

    this.setState({ tags: newTags })
  }

  render() {

    const tagsToDisplay = new Map(Array.from(this.state.tags.entries()).filter(([tag, action]) => { return action !== 'not_used' }))

    return <div style={styleHeight} >
      {this.state.error &&
        <div class="alert alert-danger" role="alert">
          {this.state.error}
        </div>
      }
      <form onSubmit={this.handleSubmit}>
        <div className="form-group">
          <label htmlFor="description">
            Décrivez en quelques mots les caractéristiques de l'objet {getTypeLabel(this.props.declarationType)} (état, couleur, nom, contenu, marque, etc...)
					</label>
          <textarea placeholder="Description de l'objet" style={{ whiteSpace: 'pre-line' }} className="form-control" id="description" name="description" rows="3" onChange={this.handleChange}></textarea>
          <button className="btn btn-warning mt-3 mb-3 p-3" type="submit">Générer les tags de recherche</button>
          <Tags className="form-control" tags={tagsToDisplay} handleTagsChanged={this.manageTagUpdating}></Tags>
        </div>
      </form></div>
  }
}

class DateAddressStep extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      thirdStep: "third step here",
      eventDate: undefined,//new Date(),
      latitude: (this.props.position && this.props.position.latitude) || null,
      longitude: (this.props.position && this.props.position.longitude) || null,
      search: undefined,
      userSelection: "",
      userLocalisationInput: undefined,
      radius: 1000
    };
    this.handleChangeDate = this.handleChangeDate.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.handler = this.handler.bind(this)

 
  }

  setposition = (position) => {
    if (position) {
      console.log('réception position :', position)
      const search = `${position.latitude},${position.longitude}`
      this.setState({ search, position, userLocalisationInput: true })
    }
  }

  isValidated() {
    if (!this.state.eventDate) {
      this.setState({ error: "Veuillez sélectionner une date." })
      setTimeout(() => this.setState({ error: undefined }), 5000)
      return false
    }
    if (!this.state.userSelection) {
      this.setState({ error: `Veuillez sélectionner une zone en remplissant "Zone de l'évènement" et en finissant par la touche "Entrer".`})
      setTimeout(() => this.setState({ error: undefined }), 10000)
      return false
    }

    const location = this.state.userSelection
    const options = { weekday: 'long', month: 'long', day: 'numeric' };
    const date = this.state.eventDate.toLocaleDateString('fr-fr', options)
    const lieu = location.split(', ').slice(0, 2).join('-')
    const label = `, le ${date} à "${lieu}"`

    this.props.calcNext(label)



    return true;
  }
  handleChangeDate(date) {
    this.setState({
      eventDate: date,
    });


    //Actuce, on va récupérer la localisation GPS à ce moment
    //Uniquement dans le cas d'une déclaration de trouvaille
    if (this.props.declarationType === 'found')
      this.setposition(this.props.getPosition())

  }

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value,
      userLocalisationInput: undefined
    });
  }

  // Récupération des donnés renvoyés par le composant MapApi
  handler(values) {
    this.setState({
      longitude: values.lostArea.y,
      latitude: values.lostArea.x,
      userInput: values.userInput,
      userSelection: values.userSelection,
    })
  }

  render() {
    return (
      <div style={styleHeight} className="container-fluid">
        {this.state.error &&
          <div class="alert alert-danger" role="alert">
            {this.state.error}
          </div>
        }
        <div className="row">
          <div className="col">
            <form onSubmit={(e) => { e.preventDefault(); this.setState({ userLocalisationInput: this.state.search }) }}>
              {!this.state.eventDate && <div className="form-group">
                <label htmlFor="dateDePerte">
                  Date de l'évènement
					</label>
                <Calendar
                  id="dateLost"
                  name="dateLost"
                  value={this.state.eventDate}
                  onChange={this.handleChangeDate}
                />
              </div>}
              {
                this.state.eventDate &&
                <div className="form-group">
                  <div className="mb-3">{capitalize(getTypeLabel(this.props.declarationType))} le :
                    <div onClick={() => this.setState({ eventDate: undefined })}>
                      {new Intl.DateTimeFormat('fr-FR').format(this.state.eventDate)}
                    </div>
                  </div>
                  <div className="row row justify-content-between">
                    <div className="col col-6">
                      <label for="exampleInputPassword1">Zone de l'évènement</label>
                      <input
                        className="form-control"
                        name="search"
                        type="text"
                        placeholder={this.state && this.state.search ? this.state.search : "ex: Lille place rihour"}
                        autoComplete="off"
                        //value={this.state.search}
                        onChange={this.handleChange}
                      />
                    </div>
                    {this.state.search &&
                      <div className="col col-6">
                        <div className="form-group">
                          <label htmlFor="search result">
                            {capitalize(getTypeLabel(this.props.declarationType))} dans un rayon (km)
                                        </label>
                          <InputRange
                            maxValue={50}
                            minValue={0}
                            value={this.state.radius / 1000}
                            onChange={value => this.setState({ radius: value * 1000 })} />
                        </div>
                      </div>
                    }
                  </div>
                </div>}

            </form>
          </div>
        </div>
        {this.state.userLocalisationInput &&
          <div className="row">
            <div className="col-md-12">
              <MapApiMisc handler={this.handler} search={this.state.search} radius={this.state.radius} position={this.props.position} />
            </div>
          </div>}
      </div>
    )
  }


  render2() {
    return (
      <div>
        <div className="row">
          <div className='col col-s12-m5'>
            <div className='col col-md-auto'>
              <div>
                <label htmlFor="dateLost">Date de perte</label>
                <Calendar
                  id="dateLost"
                  name="dateLost"
                  selected={this.state.eventDate}
                  onChange={this.handleChangeDate}
                /></div>
            </div>
            <MapApiMisc handler={this.handler} />
          </div>
        </div>
      </div>);
  }
}

class PhotoStep extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      downloadURL: undefined,
      gpsToConfirm: undefined
    }
    this.handler = this.handler.bind(this)
  }

  handler(values) {
    this.setState({
      downloadURL: values.downloadURL,
      gpsToConfirm: this.props.declarationType === 'found' ? values.gps : undefined
    })
  }

  exifAnswer = (decision) => {
    if (decision)
      this.setState({ gps: this.state.gpsToConfirm }, () => { this.props.setExifPosition(this.state.gpsToConfirm); this.props.goNext() })
    else
      this.setState({ gps: undefined, gpsToConfirm: undefined }, () => this.props.goNext())
  }


  isValidated() {
    //const label = this.state.downloadURL ? ' avec photo' : ' sans photo'
    this.props.calcNext("")
    return true;
  }

  // Intégration du composant d'Upload d'images
  render() {
    return <div className="row justify-content-md-center align-items-center" style={styleHeight} >
      <div className="col">
        <UploadImage
          handler={this.handler}
          metadata = {{declarationId:this.props.declarationId}}
          text={"Cliquez pour télécharger une image de l'objet " + getTypeLabel(this.props.declarationType)+"."}
        />
      </div>
      {this.state.gpsToConfirm &&
        <div className="col-8">

          <h5 className="title">La photo a-t-elle été prise à l'emplacement où vous avez trouvé l'objet ?</h5>
          <div className="body">
            <p>Si oui, acceptez-vous que l'on récupère la position GPS ?</p>
          </div>
          <div className="modal-footer">
            <button
              type="button"
              className="btn btn-secondary"
              onClick={() => this.exifAnswer(false)}>Non</button>
            <button
              type="button"
              className="btn btn-primary"
              onClick={() => this.exifAnswer(true)}>Oui et j'accepte</button>
          </div>
        </div>
      }
    </div>
  }
}

class NotificationStep extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      message: undefined,
      pending_request:false
    }
    this.handler = this.handler.bind(this)
  }

  requestNotfication = async () => {
    await this.setState({pending_request:true})
    const message = await requestUserNotificationPermission(this.props.firebase)
      //.then(token=> "Nous avons reçu votre autorisaton token : " + token)
      //.catch(error=> "erreur : " + error)
      .then(token => <div><p className="ml-3 p-3">Nous avons reçu votre token d'autorisation :</p>
        <p>
          <h6 className="ml-3 p-3" style={{fontSize:'.8rem'}}> {token}</h6>
        </p>
        <h2 className="text-success ml-3 p-3"> Vous pouvez valider votre déclaration.</h2>
        </div>)
      .catch(error => <div>
        <p className="ml-3 p-3">Il y a eu une erreur lors de la demande de notification.</p>
        <h2 className="text-primary ml-3 p-3"> Vous pouvez tout de même valider votre déclaration.</h2>
        </div>)


    this.setState({ message, pending_request:false })
  }

  handler(values) {
    this.setState({
      fcmId: undefined
    })
  }

  isValidated() {
    this.props.calcNext("")
    return true;
  }

  // Intégration du composant d'Upload d'images
  render() {
    const message = this.state.message
    return <div className="row justify-content-md-center align-items-center" style={styleHeight}>
      <div onClick={this.uploadClick}>
        {!message && !this.state.pending_request &&
          <h2 className="btn btn-warning text-center p-3 m-3" onClick={this.requestNotfication}>
            Cliquez pour nous permettre de vous notifier sur votre téléphone ou votre ordinateur lorsque nous pensons avoir retrouvé votre objet.
        </h2>
        }
        {message && message}
        {this.state.pending_request  &&
        <div className="spinner-border" role="status">
            <span className="sr-only">requesting...</span>
        </div>
        }

      </div>
    </div>
  }
}



class GoodDetailWizard extends React.Component {

  setStepLabel = (index, label) => {
    const stepLabelList = this.state.stepLabelList
    stepLabelList[index] = label
    this.setState({ stepLabelList }, console.log('this.state.stepLabelList', this.state.stepLabelList))
  }

  constructor(props) {
    super(props)
    this.database = props.firebase.database();
    this.state = {
      authUser: this.props.firebase.auth,
      isSend: false,
      stepLabelList: ["Déclaration d'un objet " + getTypeLabel(this.props.type), "", "", ""]
    }

    //On va créer la clé declarationId ici
    var collectionRef = this.database.ref('declarations');
    var newDocummentRef = collectionRef.push();
    const key = newDocummentRef.getKey();
    this.declarationId = key


    if (props.type === 'lost')
      this.steps = getStepsForLost.call(this)
    else
      this.steps = getStepsForFound.call(this)
  }

  componentDidMount() {
    this.listener = this.props.firebase.auth.onAuthStateChanged(
      authUser => {
        authUser
          ? this.setState({ authUser })
          : this.setState({ authUser: null })
      },
    );
  }

  componentWillUnmount() {
    this.props.firebase.auth.removeAuthTokenListener(this.listener);
  }

  // Envoi de la déclaration dans la db firebase
  addDeclaration = (declaration) => {
    try {
      const declarationId = this.declarationId
      const declarationNumber = declarationId.split('').reduce((a, b) => { a = ((a << 5) - a) + b.charCodeAt(0); return a & a }, 0)

      this.database.ref('declarations/' + declarationId)
        .set({ declarationId, declarationNumber, ...declaration }).then(() => {
          alert('Votre déclaration a bien été prise en compte !')
          console.log('this', this)
          this.goHome.click()
        })
    } catch (e) {
      console.log('erreur dans addDeclaration', e)
    }
    

  }

  // Construction de l'objet déclaration qui va être envoyé en db
  finishButtonClick = (allStates) => {
    try {
      //Date de l'évenemment 
      var d = allStates['Date et lieu'].eventDate
      var eventStringDate = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear() + ' ' + d.toLocaleTimeString('fr-FR')
      var eventNumericDate = d.getTime()

      //Date de la déclaration
      var today = new Date()
      var declarationStringDate = (today.getMonth() + 1) + '/' + today.getDate() + '/' + today.getFullYear() + ' ' + d.toLocaleTimeString('fr-FR')
      var declarationNumericDate = today.getTime()

      var user = this.props.firebase.auth.currentUser

      var type = this.props.type

      var photoLatitude = null
      var photoLongitude = null

      if (allStates.Photo && typeof allStates.Photo.gps !== 'undefined') {
        photoLatitude = allStates.Photo.gps.latitude
        photoLongitude = allStates.Photo.gps.longitude
      }
      const eventDate = allStates['Date et lieu']

      const tags = Array.from(allStates.Description.tags.entries()).filter(([tag, action]) => { return { tag, action } })

      var declaration = {
        "declarationType": type,
        "status": "pending",
        "category": allStates.Catégorie.category,
        "message": type === 'found' ? allStates.Message.message : "",
        "matchingList": {
          'items': null
        },
        "decisionOrigin": null,
        "notification": {
          "sendingDate": null,
          "status": "not_send",
          "metadata": null
        },
        "userDescription": allStates.Description.description,
        "tagsHigh": [],
        "tags": tags,
        "eventDate": {
          "numericDate": eventStringDate,
          "stringDate": eventNumericDate
        },
        "declarationDate": {
          "numericDate": declarationStringDate,
          "stringDate": declarationNumericDate
        },
        "eventZone": {
          "userInput": eventDate.userInput,
          "userSelection": eventDate.userSelection,
          "searchRadius": eventDate.radius,
          "userSelectionArea": {
            "latitude": parseFloat(eventDate.latitude),
            "longitude": parseFloat(eventDate.longitude)
          },
          "exifGPS": {
            "latitude": photoLatitude,
            "longitude": photoLongitude
          },
          "searchArea": {
            "latitude": photoLatitude,
            "longitude": photoLongitude
          }
        },
        "declarationOwner": {
          "FullName": user.displayName,
          "email": user.email,
          "userId": user.uid
        },
        "imageURL": allStates.Photo && allStates.Photo.downloadURL ? allStates.Photo.downloadURL : "",
        "metadata": {
          "creationDate": {
            "numericDate": declarationStringDate,
            "stringDate": declarationNumericDate
          },
          "lastUpdateDate": {
            "numericDate": declarationStringDate,
            "stringDate": declarationNumericDate
          },
          "lastUser": {
            "FullName": user.displayName,
            "email": user.email
          }
        }
      }
      this.addDeclaration(declaration)
    } catch (error) {
      console.error(error)
    }


  }

  render() {
    const stepLabelList = this.state.stepLabelList
    if (this.state.authUser)
      return (
        <Container fluid="true" className="wizard" style={{ flex: 1, alignContent: "center" }}>
          <Row>
            <Col xs={12} md={12} className="mr-auto ml-auto">
              <ReactWizard
                color="primary"
                steps={this.steps}
                navSteps
                title={<nav aria-label="breadcrumb">
                  <ol className="breadcrumb" style={{fontSize:"1rem"}}>
                    <li >{stepLabelList[0]}</li>
                    <li className="" >{stepLabelList[1]}</li>
                    <li className="" >{stepLabelList[2]}</li>
                    <li className="" aria-current="page">{stepLabelList[3]}</li>
                  </ol>
                </nav>}
                headerTextLeft
                validate
                progressbar="false"
                previousButtonClasses="btn btn-success"
                nextButtonClasses="btn btn-success"
                finishButtonClasses="btn btn-success"
                finishButtonText="Valider"
                nextButtonText="Suivant"
                previousButtonText="Précédent"
                //previousButtonClasses="d-none"
                //nextButtonClasses="d-none"
                nextButtonText={<div ref={input => this.boutonNext = input}>Suivant</div>}
                finishButtonClick={this.finishButtonClick}
              />
            </Col>
          </Row>
            <NavLink to={ROUTES.HOME} ref={input =>  this.goHome = input } />
        </Container>
      );
    else
      return ('Vous devez vous connecter pour déclarer un objet perdu')
  }
}

function getStepsForLost() {
  return [

    {
      stepName: "Catégorie",
      component: CategorySelection,
      stepProps: {
        declarationType: this.props.type,
        calcNext: (stepLabel) => this.setStepLabel(0, stepLabel),
        goNext: () => this.boutonNext.click()
      }
    },
    {
      stepName: "Date et lieu",
      component: DateAddressStep,
      stepProps: {
        declarationType: this.props.type,
        calcNext: (stepLabel) => this.setStepLabel(1, stepLabel)
      }
    },
    {
      stepName: "Description",
      component: DescriptionStep,
      stepProps: {
        declarationType: this.props.type,
        calcNext: (stepLabel) => this.setStepLabel(2, stepLabel)
      }
    },
    {
      stepName: "Photo",
      component: PhotoStep,
      stepProps: {
        declarationType: this.props.type,
        declarationId : this.declarationId,
        calcNext: (stepLabel) => this.setStepLabel(3, stepLabel)
      }
    },
    {
      stepName: "Notification",
      component: NotificationStep,
      stepProps: {
        declarationType: this.props.type,
        calcNext: (stepLabel) => this.setStepLabel(4, stepLabel),
        firebase: this.props.firebase
      }
    },
  ];
}
//Component specific pour les objets trouvés

function getStepsForFound() {
  return [
    {
      stepName: "Catégorie",
      component: CategorySelection,
      stepProps: {
        declarationType: this.props.type,
        declarationId : this.declarationId,
        calcNext: (stepLabel) => this.setStepLabel(0, stepLabel),
        goNext: () => this.boutonNext.click()
      }
    },
    {
      stepName: "Photo",
      component: PhotoStep,
      stepProps: {
        declarationType: this.props.type,
        declarationId : this.declarationId,
        calcNext: (stepLabel) => this.setStepLabel(1, stepLabel),
        askPermissionExif: true,
        setExifPosition: (position) => this.setState({ exifPosition: position }),
        goNext: () => this.boutonNext.click()
      }
    },
    {
      stepName: "Date et lieu",
      component: DateAddressStep,
      stepProps: {
        declarationType: this.props.type,
        calcNext: (stepLabel) => this.setStepLabel(2, stepLabel),
        position: this.state.exifPosition,
        getPosition: () => this.state.exifPosition
      }
    },
    {
      stepName: "Description",
      component: DescriptionStep,
      stepProps: {
        declarationType: this.props.type,
        calcNext: (stepLabel) => this.setStepLabel(3, stepLabel)
      }
    },
    {
      stepName: "Message",
      component: MessageStep,
      stepProps: {
        declarationType: this.props.type,
        calcNext: (stepLabel) => this.setStepLabel(4, stepLabel),
        firebase: this.props.firebase
      }
    },
  ];
}

function getTypeLabel(type) { return type === 'lost' ? 'perdu' : 'trouvé' }
function capitalize(str1) {
  return str1.charAt(0).toUpperCase() + str1.slice(1);
}

export default withFirebase(GoodDetailWizard)