import React from 'react'
import ReactTags from 'react-tag-autocomplete'
import './Tags.css'

export default class Tags extends React.Component{
constructor(props){
    super(props)
    this.state= {tags: new Map(props.tags)}
    this.handleDelete = this.handleDelete.bind(this);
    this.handleAddition = this.handleAddition.bind(this);
    this.handleDrag = this.handleDrag.bind(this);
    this.handleKeyPress= this.handleKeyPress(this);

}


handleDelete(i) {
    const { tags } = this.state;
    const newTags = new Map(tags)
    const [keyToRemove] = Array.from(tags.keys()).filter((tag, index) => index === i)
    this.setState({tags:newTags});
    this.props.handleTagsChanged([keyToRemove, 'not_used'])
}

handleAddition(tag) {
//Ne fnctionne pas    const newTags =  [...this.state.tags, tag]
//    this.setState({tags:newTags});
 //   this.props.handleTagsChanged({tags : newTags, action:'ad'})
}

handleKeyPress(keyPress) {
   /* console.log('keyPress', keyPress)
    if (delimiters.indexOf(keyPress))
        return this.handleAddition (this.state.currentTagEntering)
        
    this.setState = {currentTagEntering : this.state.currentTagEntering + keyPress}
    */
}

handleDrag(tag, currPos, newPos) {
    const tags = [...this.state.tags];
    const newTags = tags.slice();

    newTags.splice(currPos, 1);
    newTags.splice(newPos, 0, tag);

    // re-render
    this.setState({ tags: newTags });
}

UNSAFE_componentWillReceiveProps(newProps){
    this.setState({tags:newProps.tags})
}

render(){
    const {tags} = this.state;
    const tagsResult = Array.from(tags.keys()).map((tag,index) => {return {id:tag, name:tag}})

    const suggestions = [{id:1, name:'a'}]
    return(
        <div>
            
<div>
                <ReactTags tags={tagsResult}
                    suggestions={suggestions}
                    handleDelete={this.handleDelete}
                    handleAddition={this.handleAddition}
                    handleDrag={this.handleDrag}
                    handleInputChange= {this.handleKeyPress}
                    addOnBlur={true}
                    />
            </div>

        </div>
    )
}
}