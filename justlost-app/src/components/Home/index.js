import React, { Component } from "react";
import { withFirebase } from "../Firebase";

class Home extends Component {

  constructor(props) {
    super(props)
    this.state = {
      authUser: props.firebase.auth,
      isLoading: false,
    }
  }

  componentWillMount() {
    this.setState({
      isLoading: true
    })

    this.props.firebase.database().ref(`declarations`)
    .orderByKey()
    .on('value', async (snap) => {
      try {
        let allDeclarations
        if (snap.exists())
          allDeclarations = await Object.values(snap.val()).reverse()
        else
          allDeclarations = []


        var userDeclarationsFoundList = []
        var userDeclarationsLostList = []

        allDeclarations.forEach(element => {
          if ((typeof element.declarationOwner != 'undefined') && element.declarationOwner.email === this.props.firebase.auth.currentUser.email) {
            if (element.declarationType === 'found')
              userDeclarationsFoundList.push(element)
            if (element.declarationType === 'lost')
              userDeclarationsLostList.push(element)
          }
        });

        const foundList = userDeclarationsFoundList.map((declaration) =>
          <div className="card mb-3">
            <div>
              <div className="responsive-image">
              <img src={declaration.thumbnailURL || declaration.imageURL} className="card-img-top" alt=""></img>
              </div>
              <div>
                <div className="card-body">
                  <h5 className="card-title">Trouvé: {declaration.category.label} le {declaration.eventDate.numericDate}</h5>
                  <p> vers {declaration.eventZone.userSelection}</p>
                  <p className="card-text">{declaration.userDescription}</p>
                </div>
                {declaration.status !== 'matched' && <div className="card-body">
                  {declaration.status === 'cancelled' && <p className="text-danger">Déclaration annulée</p>}
                  {declaration.status !== 'cancelled' && <button className="btn btn-danger" onClick={() => this.handleCancel(declaration.declarationId)}>Annuler la déclaration</button>}
                </div>}
                {declaration.status === 'matched' && <div className="card-body">
                  <div class="alert alert-success" role="alert">
                    <p>Nous avons retrouvé le propriétaire de cet objet il devrait vous contacter rapidement !</p>
                  </div>
                </div>}
              </div>
            </div>
          </div>
        )

        const lostList = userDeclarationsLostList.map((declaration) =>
          <div className="card mb-3">
            <div>
              <div className="responsive-image">
              <img src={declaration.thumbnailURL || declaration.imageURL} className="card-img-top" alt=""></img>
              </div>
              <div>
                <div className="card-body">
                  <h5 className="card-title">Perdu: {declaration.category.label} le {declaration.eventDate.numericDate}</h5>
                  <p> vers {declaration.eventZone.userSelection}</p>
                  <p className="card-text">{declaration.userDescription}</p>
                </div>
                {declaration.status !== 'matched' && <div className="card-body">
                  {declaration.status === 'cancelled' && <div class="alert alert-danger" role="alert">Déclaration annulée</div>}
                  {declaration.status !== 'cancelled' && <button className="btn btn-danger" onClick={() => this.handleCancel(declaration.declarationId)}>Annuler la déclaration</button>}
                </div>}
                {declaration.status === 'matched' && <div className="card-body">
                  <div class="alert alert-success" role="alert">
                    <p>{declaration.foundDetail.fullName} a retrouvé votre objet, contactez le: {declaration.foundDetail.email}</p>
                    <hr></hr>
                    <p>Son message: "{declaration.foundDetail.message}"</p>
                  </div>
                </div>}
              </div>
            </div>
          </div>
        )

        this.setState({
          userDeclarationsLostList: lostList,
          displayLost: (userDeclarationsLostList.length > 0),
          userDeclarationsFoundList: foundList,
          displayFound: (userDeclarationsFoundList.length > 0),
          isLoading: false,

        })
        return Promise.resolve('OK')
      } catch (e) {
        return Promise.reject(e)
      }
    })
  }

  handleCancel(declarationId) {
    this.props.firebase.database().ref(`declarations/${declarationId}`).update({ status: 'cancelled' })
  }

  componentDidMount() {
    this.listener = this.props.firebase.auth.onAuthStateChanged(
      authUser => {
        authUser
          ? this.setState({ authUser })
          : this.setState({ authUser: null })
      },
    );
  }

  render() {

    if (this.state.authUser) {
      return (
        <div>
          <div className="container-fluid flex-wrap">
            {((this.state.displayFound === false) && (this.state.displayLost === false)) && <div className="alert alert-secondary" role="alert">
              Aucune déclaration à afficher.
            </div>}
            <div>
              {this.state.isLoading && <strong>Chargement...</strong>}
              {!this.state.isLoading && <div className="row row-cols-2 d-flex bg-light rounded">
                {this.state.displayLost && <div className="col-md-6 col-12" >
                  <h4><span class="badge badge-secondary">Objets perdus</span></h4>
                  {this.state.userDeclarationsLostList}
                </div>}
                {this.state.displayFound && <div className="col-md-6 col-12" >
                  <h4><span class="badge badge-secondary">Objets trouvés</span></h4>
                  {this.state.userDeclarationsFoundList}
                </div>}
              </div>}
            </div>
          </div>
        </div>
      )
    }
    else {
      return (
        <div className="container">
          <div className="alert alert-secondary" role="alert">
            Vous devez être connecté pour accéder à la Home page.
            </div>
        </div>
      )
    }
  }
}

export default withFirebase(Home)