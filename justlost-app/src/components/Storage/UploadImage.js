import React from 'react';
import { getExifGPS } from '../../helpers/getExifGPS'
import { withFirebase } from '../Firebase'
import './UploadImage.css'

class UploadImage extends React.Component {
    constructor(props) {
        //Obligatoire quand on surcharge le constructeur
        super(props)
        this.state = { percentUpdload: 0 }
        //Je récupère la db et la stocke au niveau de mon object pour une faciliter d'écriture
        this.storage = props.firebase.storage;

        this.uploadImage = this.uploadImage.bind(this)
        this.handleFileSelected = this.handleFileSelected.bind(this)
    }

    async uploadImage(file = this.state.fileToUpdate) {
        let newFileName = Date.now()+ '_' + file.name  
        var metadata ={}
        var customMetadata = {}

        // Create a root reference
        var storageRef = this.storage.ref();

        const gps = await getExifGPS(file)
        if (gps) {
            this.setState({ gps })
            customMetadata = {
                    gps_latitude: gps.latitude,
                    gps_longitude: gps.longitude
            };
        }

        metadata = { customMetadata : { ...this.props.metadata, ...customMetadata, originalFileName : file.name, storageFileName: newFileName }}
        var imageRef = storageRef.child(newFileName)

        //Ajouter le test du type if (!contentType.startsWith('image/')) {

        const uploadTask = imageRef.put(file, metadata)
        uploadTask.on('state_changed', (snapshot) => {
            // Observe state change events such as progress, pause, and resume
            // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
            var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            this.setState({ percentUpdload: progress })
        }, function (error) {
            // Handle unsuccessful uploads
            this.setState({ error })
        }, () => {
            // Handle successful uploads on complete
            console.log('imageRef', imageRef)
            imageRef.getDownloadURL().then(downloadURL => {
                let data = {}
                if (gps)
                    data.gps = { latitude: gps.latitude, longitude: gps.longitude }
                data.downloadURL = downloadURL
                data.imageFilename = file

                this.setState(data)
                this.props.handler(data)

            });
        });
    }

    handleFileSelected(e) {
        this.setState({
            [e.target.id]: e.target.files[0]
        })
        this.uploadImage(e.target.files[0])
    }
    uploadClick = e => {
        e.preventDefault();
        console.log("clicked")
        this.inputFile.click();
        return false;
    };
    render() {
        return (
            <div className="row bg-light border rounded p-2 align-self-center" style={{ margin: 'auto', height: '400px', width: '80%' }}>
                <div className="d-none">
                    <input type="file"
                        id="fileToUpdate"
                        accept=".jpg, .png, .heif, .heic"
                        onChange={this.handleFileSelected}
                        ref={input => { this.inputFile = input }}
                    />
                </div>
                <div className="col align-self-center" onClick={this.uploadClick}>
                    {!this.state.downloadURL && this.state.percentUpdload <=0 && <p className="text-center" >{this.props.text}</p>}
                    {this.state.percentUpdload > 0 && this.state.percentUpdload < 100 &&
                        <div className="row">
                            <div className="col mb-3">
                                <div className="progress">
                                    <div className="progress-bar danger" role="progressbar" style={{ width: this.state.percentUpdload + '%' }} aria-valuemin="0" aria-valuemax="100">{Math.round(this.state.percentUpdload, 1)}</div>
                                </div>
                            </div>
                        </div>}
                    {this.state.downloadURL &&
                        <div className="thumbnail ">
                            <img
                                alt=""
                                className="img-fluid rounded image-cat mb-2 p-1 shadow bg-white rounded"
                                style={{ width: '100%'}}
                                src={this.state.downloadURL}>
                            </img>
                        </div>}
                </div>
            </div>)
    }
}

export default withFirebase(UploadImage)