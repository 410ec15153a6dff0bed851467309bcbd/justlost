import React from 'react';

import { withFirebase } from '../Firebase';
import history from '../History'
import { NavLink } from 'react-router-dom';
import * as ROUTES from '../../constants/routes';

class Navigation extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      authUser: this.props.firebase.auth,
    }
  }

  componentDidMount() {

    this.listener = this.props.firebase.auth.onAuthStateChanged(
      authUser => {

        if (authUser) {

          if (this.props.firebase.auth.currentUser != null) {

            let uid = this.props.firebase.auth.currentUser.uid

            let isUserAdmin = async () => {
              await this.props.firebase.database().ref(`users/${uid}/isAdmin`).on('value', (snap) => {
                this.setState({
                  isAdmin: snap.val()
                })
              })
            }
            isUserAdmin();

            console.log('isAdmin', this.state.isAdmin)

            this.setState({
              authUser,
              user: this.props.firebase.auth.currentUser
            })
          }
        }
        else {
          this.setState({
            authUser: null,
            isAdmin: false
          })
        }
      },
    );
  }

  componentWillUnmount() {
    this.listener.off()
  }

  render() {
    const activeStyle = {
      //background:'red',
      color: '#007bff'
    }

    return (<nav className="navbar justify-content-between navbar-expand navbar-dark rounded" style={{ backgroundColor: "#f58220" }}>

      {this.state.authUser &&
        <NavLink to={ROUTES.HOME} className="nav-link nav-item justify-content-between align-items-center d-none d-sm-block">
          <span className="d-lg-inline-block " style={{ fontSize: "1.5rem" }}>JustLost</span>
        </NavLink>
      }
      
      {!this.state.authUser &&
      <div>
      <span className="d-lg-inline-block text-primary" style={{ fontSize: "1.5rem" }}>JustLost</span>
      </div>
        
      }


      <div className="navbar-nav justify-content-between align-items-center" id="navbarNavAltMarkup">

        <div className="navbar-nav">
          {this.state.authUser && <div className="nav-item">
            <NavLink to={ROUTES.DECLARE_LOST_GOOD} activeStyle={activeStyle} className="nav-link">Perdu</NavLink>
          </div>}


          {this.state.authUser && <div className="nav-item">
            <NavLink to={ROUTES.DECLARE_FOUND_GOOD} activeStyle={activeStyle} className="nav-link">Trouvé</NavLink>
          </div>}
        </div>
      </div>

      <div className="navbar-nav justify-content-end align-items-center" id="navbarNavAltMarkup">


        <div className="navbar-nav ml-auto my-2 mv-3">
          <NavLink to={ROUTES.QRCODE} activeStyle={activeStyle} className="nav-link nav-item justify-content-between align-items-center d-none d-sm-block" >
              <i className="fa fa-qrcode fa-2x" aria-hidden="true"></i>
            </NavLink>

          {this.state.authUser &&
            <NavLink to={ROUTES.HOME} activeStyle={activeStyle} className="nav-link nav-item justify-content-between align-items-center" >


              <i className="fa fa-home fa-2x" aria-hidden="true"></i>

            </NavLink>
          }

          {this.state.isAdmin && <li className="nav-item">
            <NavLink to={ROUTES.EXEMPLES} activeStyle={activeStyle} className="nav-link d-none d-sm-block">
              <i className="fa fa-asterisk fa-2x" aria-hidden="true"></i>
            </NavLink>
          </li>}
          {this.state.isAdmin && <li className="nav-item">
            <NavLink to={ROUTES.ADMIN_DECLARATION_LIST} activeStyle={activeStyle} className="nav-link d-none d-sm-block">
              <i className="fa fa-user fa-2x" aria-hidden="true"></i>
            </NavLink>
          </li>}
          {!this.state.authUser && <div className="nav-item">
            <NavLink to={ROUTES.AUTHENTIFICATION} activeStyle={activeStyle} className="nav-link"> Connexion</NavLink>
          </div>}
          {this.state.authUser &&
            <div className="nav-link nav-item" onClick={async () => {
              await this.props.firebase.auth.signOut()
              this.setState({
                user: null,
                isAdmin: false
              })
              history.push('/authentification')
            }}
            >
              <i className="fa fa-sign-out fa-2x" aria-hidden="true"></i>
            </div>
          }
        </div>
      </div>
    </nav>)

    return (
      <nav className="navbar navbar-expand navbar-light bg-light">
        <a className="navbar-brand" href="#">JustLost</a>

        <div className="nvabar" id="">
          <ul className="navbar-nav d-flex justify-content-center">
            {this.state.authUser && <li className="nav-item">
              <NavLink to={ROUTES.DECLARE_LOST_GOOD} activeStyle={activeStyle} className="nav-link">Perdu !</NavLink>
            </li>}
            {this.state.authUser && <li className="nav-item">
              <NavLink to={ROUTES.DECLARE_FOUND_GOOD} activeStyle={activeStyle} className="nav-link">Trouvé !</NavLink>
            </li>}
            {this.state.authUser && <li className="nav-item">
              <NavLink to={ROUTES.HOME} activeStyle={activeStyle} className="nav-link" >

                <span className="d-none d-sm-inline">Home</span>
                <i className="fa fa-home" aria-hidden="true"></i>

              </NavLink>
            </li>}
            {this.state.isAdmin && <li>
              <NavLink to={ROUTES.EXEMPLES} activeStyle={activeStyle} className="nav-link">Exemples</NavLink>
            </li>}

            {this.state.isAdmin && <li className="nav-item">
              <NavLink to={ROUTES.ADMIN_DECLARATION_LIST} activeStyle={activeStyle} className="nav-link"> Administration</NavLink>
            </li>}
            {!this.state.authUser && <li className="nav-item">
              <NavLink to={ROUTES.AUTHENTIFICATION} activeStyle={activeStyle} className="nav-link"> Connexion</NavLink>
            </li>}
            {this.state.authUser && <li className="nav-item">
              <div className="nav-link d-flex flex-column" onClick={async () => {
                await this.props.firebase.auth.signOut()
                this.setState({
                  user: null,
                  isAdmin: false
                })
                history.push('/authentification')
              }}
              >
                <span className="d-none d-sm-inline">Déconnexion</span>
                <i className="fa fa-sign-out" aria-hidden="true"></i>
              </div>
            </li>}
          </ul>
        </div>
      </nav>
    )
  }
}

export default withFirebase(Navigation);