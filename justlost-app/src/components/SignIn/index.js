import React from 'react';
import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth';
import firebase from 'firebase';
import * as ROUTE from '../../constants/routes'
import { withFirebase } from '../Firebase'


class SignIn extends React.Component {

// Configure FirebaseUI.
  uiConfig = {
  // Popup signin flow rather than redirect flow.
  signInFlow: 'popup',
  // Redirect to /home after sign in is successful. Alternatively you can provide a callbacks.signInSuccess function.
  signInSuccessUrl: ROUTE.HOME,
  // List of choosen auth providers.
  signInOptions: [
    firebase.auth.GoogleAuthProvider.PROVIDER_ID,
    firebase.auth.EmailAuthProvider.PROVIDER_ID

  ]
};

  render() {

    if (this.props.firebase.auth.currentUser == null) {
      return (
        <div>
          <StyledFirebaseAuth uiCallback={ui => ui.disableAutoSignIn()} uiConfig={this.uiConfig} firebaseAuth={this.props.firebase.auth}/>
        </div>
      );
    }
    else
    {
      return (
      <div>
        <p>Vous êtes déjà connecté</p>
      </div>);
    }
  }
}

export default withFirebase(SignIn)