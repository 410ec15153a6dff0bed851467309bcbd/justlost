import React, { Component } from "react"
import { Map, TileLayer, Circle } from "react-leaflet"
import axios from "axios"
import "./personalStyle.css"
import 'react-input-range/lib/css/index.css'

class mapApiMisc extends Component {

    constructor(props) {
        super(props)

        this.state = {
            lostAreaX: 0,           // Coords of the lost area
            lostAreaY: 0,           // Also used for the preview
            search: "",             // The input search
            searchResult: [],       // Will be filled with api result, as an array of json
            searchResultActive: "",  // Value of the current selection
            searchResultName: ""
        }
        this.onChange = this.onChange.bind(this)
        this.onSubmit = this.sendItem.bind(this)

        this.onSearchLocation = this.onSearchLocation.bind(this)

        this.listResult = this.listResult.bind(this)
        this.onChangeChooseResult = this.onChangeChooseResult.bind(this)
    }

    /* change/submit basic function */
    onChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }
    sendItem() {
        const item = {
            lostArea: {
                x: this.state.lostAreaX,
                y: this.state.lostAreaY
            },
            userInput: this.props.search,
            userSelection: this.state.searchResultName
        }

        /* Store the new item in the database */
        this.props.handler(item)
    }

    /* search section function */
    onSearchLocation() {
        let search = this.props.search;
        let searchQuery
        const givenPosition = this.props.position
        if (givenPosition)
            searchQuery = `lat=${givenPosition.latitude}&lon=${givenPosition.longitude}`
        else
            searchQuery = `search?q=${search}`


        axios
            .get(
                `https://nominatim.openstreetmap.org/${searchQuery}&format=json`
            )
            .then((res) => {
                if (res.data.length === 0) {
                    console.log("no result for " + search)
                } else {
                    let arrOfResponse = res.data.map(currentResponse => {
                        return (
                            {
                                name: currentResponse.display_name,
                                osmId: currentResponse.osm_id,
                                x: currentResponse.lat,
                                y: currentResponse.lon
                            }
                        )
                    })
                    this.setState({
                        searchResult: arrOfResponse,      // Set the array of result in state
                        lostAreaX: arrOfResponse[0].x,    // From the array of result, set the preview
                        lostAreaY: arrOfResponse[0].y,     // with the coords of the first result
                        searchResultName: arrOfResponse[0].name
                    })
                    this.sendItem()
                }
            })
            .catch((err) => {
                console.log(err)
            })
    }

    /* select/option tag function */
    listResult() {
        // Fill the select with formated option tag
        return this.state.searchResult.map((currentResult, index) => {
            let idXY = currentResult.name + "&" + currentResult.x + "&" + currentResult.y
            return (
                <option
                    key={index}
                    value={idXY}
                >
                    {currentResult.name}
                </option>
            )
        })
    }

    onChangeChooseResult(event) {
        let arrOfParams = event.target.value.split("&")
        let name = arrOfParams[0]
        let coordX = parseFloat(arrOfParams[1], 10)
        let coordY = parseFloat(arrOfParams[2], 10)

        this.setState({
            searchResultActive: event.target.value,
            lostAreaX: coordX,
            lostAreaY: coordY,
            searchResultName: name
        })

        this.sendItem()
    }

    componentDidMount() {
        this.onSearchLocation()
    }

    render() {

        const searchResultDisabled = (
            <select
                className="form-control"
                disabled={true}
            >
                <option>Recherchez une adresse ci-dessus</option>
            </select>
        )

        const searchResult = (
            <select
                className="form-control"
                value={this.state.searchResultActive}
                onChange={this.onChangeChooseResult}
            >
                {this.listResult()}
            </select>
        )

        const searchResultMap = (
            <Map
                center={[
                    this.state.lostAreaX,
                    this.state.lostAreaY
                ]}
                zoom={12}
                scrollWheelZoom={false}
            >
                <Circle
                    center={[
                        this.state.lostAreaX,
                        this.state.lostAreaY
                    ]}
                    radius={this.props.radius }
                >
                    <TileLayer
                        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    />
                </Circle>
            </Map>
        )

        return (
            <div>

                {/* Leaflet need its style and script */}
                <link
                    rel="stylesheet"
                    href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
                    integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
                    crossOrigin=""
                />
                <script
                    src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
                    integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
                    crossOrigin=""
                ></script>
                {/* End of needed things */}

                <div className="row">
                    <div className="col">

                        <form noValidate onSubmit={this.onSubmit}>

                            {/* Map input */}
                            <div className="row">
                                {/* sub search */}


                                {/* result search */}
                                <div className="col-md-auto">
                                    <div className="form-group">
                                        <label htmlFor="search result">
                                            Précisez le lieu
                                        </label>

                                        {
                                            this.state.searchResult.length === 0
                                                ?
                                                searchResultDisabled
                                                :
                                                searchResult
                                        }

                                    </div>
                                </div>
                            </div>
                            
                            {/* Map preview */}
                            <div className="row">
                                <div className="col">
                                    {
                                        this.state.searchResult.length === 0
                                            ?
                                            ""
                                            :
                                            searchResultMap
                                    }
                                </div>
                            </div>

                            {/*<button
                                type="submit"
                                className="btn btn-lg btn-primary btn-block"
                            >
                                Valider
                            </button>*/}

                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default mapApiMisc