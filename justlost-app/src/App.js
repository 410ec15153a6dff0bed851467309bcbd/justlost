import React from 'react';
import ExempleList from './pages/exemples/ExempleList'
import SignIn from './components/SignIn'
import Home from './components/Home'
import Navigation from './components/Navigation'
import QrCodePage from './pages/QrCode/QrCodePage'
//import HomePage from './pages/homepage/HomePage'
import AdminPage from './pages/admin/AdminPage'
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom';
import './App.css';
import * as ROUTES from '../src/constants/routes';
import GoodDetailWizard from '../src/components/goods/GoodDetailWizard'
import history from './components/History';
//      <Route path={ROUTES.HOME_NEW} component={HomePage} />      
const App = () =>
<div className="container">
    <Router history={ history }>
      <Navigation />
      <Route exact path={ROUTES.HOME} component={Home} />
      <Route path={ROUTES.AUTHENTIFICATION} component={SignIn} />
      <Route path={ROUTES.EXEMPLES} component={ExempleList} />
      <Route path={ROUTES.DECLARE_LOST_GOOD} component={() => <GoodDetailWizard type='lost'/>} />
      <Route path={ROUTES.DECLARE_FOUND_GOOD} component={() => <GoodDetailWizard type='found'/>} />
      <Route path={ROUTES.ADMIN_DECLARATION_LIST} component={AdminPage} />      
      <Route path={ROUTES.QRCODE} component={() => QrCodePage} />
    </Router>
  </div>

export default App;
