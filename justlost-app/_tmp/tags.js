Array.prototype.unique = function() {
    return this.filter(function (value, index, self) { 
      return self.indexOf(value) === index;
    });
  }

const str="J'ai perdu ma veste et veste et veste Salomon Bonatti WP rouge et bordeaux avec son zip jaune pour homme"

excludedWords=["j'ai", "ma", "et", "avec", "pour","du", "de", "des", "un", "une", "la", "le", "en", "à", "son", "sa", "ne", "pas", "vous", ".", "merci", ',']
const rawWords = str.split(' ');

strAll = rawWords.filter(word => excludedWords.indexOf(word.toLowerCase())===-1 && word.length>1)
strAll = strAll.unique()

console.log(strAll)