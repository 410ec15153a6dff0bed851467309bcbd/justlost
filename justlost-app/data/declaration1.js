const decl = 
{
    category: {
      code: 'phone',
      id: 5,
      image: '/static/media/telephone.55c81c32.jpg',
      label: 'Electronique'
    },
    decisionOrigin: {
      description: 'Origine de la décision',
      enum: [Array],
      type: 'string'
    },
    declarationDate: { numericDate: '5/20/2020', stringDate: 1589986460559 },
    declarationId: { description: 'Id de la déclaration', type: 'string' },
    declarationOwner: {
      FullName: 'Norbert POINTU',
      email: 'norbert_pointu@hotmail.com',
      messengerID: [Object],
      userId: [Object]
    },
    declarationType: 'lost',
    eventDate: { numericDate: '5/6/2020', stringDate: 1588716000000 },
    eventZone: {
      exifGPS: [Object],
      searchArea: [Object],
      searchRadius: [Object],
      userInput: 'A lille',
      userSelection: [Object],
      userSelectionArea: [Object]
    },
    imageURL: 'https://firebasestorage.googleapis.com/v0/b/justlost-f169e.appspot.com/o/telephone.jpg?alt=media&token=0e6b8a2a-e6f8-47af-a631-df59f6a94df9',
    metadata: {
      creationDate: [Object],
      lastUpdateDate: [Object],
      lastUser: [Object],
      oid: [Object]
    },
    notification: { status: 'not_send' },
    status: 'pending',
    userDescription: "J'étais dans un bar, j'ai bu quelques bieres et puis ... plus de téléphone iphone 11sPlus",
    id: '-M7m_-Wbk4QrbfAnv3ss'
  }